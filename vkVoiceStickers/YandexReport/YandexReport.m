//
//  YandexReport.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 09.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "YandexReport.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>


@implementation YandexReport

+ (void)reportEventWithName:(NSString *)name
{
    [YMMYandexMetrica reportEvent:name onFailure:^(NSError *error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }];
}

+ (void)reportEventWithName:(NSString *)name index:(int)index
{
    NSString *message = [NSString stringWithFormat:@"EVENT-%@ %d", name, index];
    [[self class] reportEventWithName:message];
}

+ (void)reportErrorWithName:(NSString *)name exception:(NSException *)exception
{
    [YMMYandexMetrica reportError:name exception:exception onFailure:^(NSError *error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }];
}

+ (void)reportEventWithName:(NSString *)name parameters:(NSDictionary *)params
{
    [YMMYandexMetrica reportEvent:name parameters:params onFailure:^(NSError *error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }];
}

+ (void)reportEventCancelAuth
{
    static int counter = 0;
    ++counter;
    [[self class] reportEventWithName:@"CancelAuth" index:counter];
}

+ (void)reportEventAuth
{
    static int counter = 0;
    ++counter;
    [[self class] reportEventWithName:@"Auth" index:counter];
}

+ (void)reportError
{
    static int counter = 0;
    ++counter;
    NSString *name = [NSString stringWithFormat:@"ERROR %d", counter];
    
    [[self class] reportErrorWithName:name exception:nil];
}

+ (void)reportErrorWithName:(NSString *)name
{
    static int counter = 0;
    ++counter;
    
    [[self class] reportErrorWithName:name exception:nil];
}

+ (void)reportException
{
    static int counter = 0;
    ++counter;
    NSString *name = [NSString stringWithFormat:@"EXCEPTION %d", counter];
    
    NSException *testException = [NSException exceptionWithName:name
                                                         reason:@"test exception"
                                                       userInfo:nil];
    @try {
        [testException raise];
    }
    @catch (NSException *exception) {
        [[self class] reportErrorWithName:name exception:exception];
    }
    @finally {
        testException = nil;
    }
}

@end
