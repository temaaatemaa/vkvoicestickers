//
//  YandexReport.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 09.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YandexReport : NSObject


+ (void)reportEventCancelAuth;
+ (void)reportEventAuth;
+ (void)reportEventWithName:(NSString *)name parameters:(NSDictionary *)params;
+ (void)reportError;
+ (void)reportErrorWithName:(NSString *)name;
+ (void)reportException;

@end
