//
//  VkUIDelegate.h
//  test2
//
//  Created by Artem Zabludovsky on 11.04.17.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VKSdk.h>

@interface VkUIDelegate : NSObject <VKSdkUIDelegate>
@property UIViewController* vc;

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller;
- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError;
- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result;
@end
