//
//  VkDelegate.h
//  test2
//
//  Created by Artem Zabludovsky on 11.04.17.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VKSdk.h>

@interface VkDelegate : NSObject <VKSdkDelegate>

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result;
- (void)vkSdkUserAuthorizationFailed;
- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError;
- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken;
- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken;
@end
