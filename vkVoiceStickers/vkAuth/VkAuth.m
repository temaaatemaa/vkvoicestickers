//
//  VkAuth.m
//  test2
//
//  Created by Artem Zabludovsky on 11.04.17.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import "VkAuth.h"

#import "VkDelegate.h"
#import "VkUIDelegate.h"

#define YOUR_APP_ID @"6400276"

@implementation VkAuth

- (void)autentification: (UIViewController *)vc
{
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:YOUR_APP_ID];
    VkDelegate *delegate = [[VkDelegate alloc]init];
    VkUIDelegate *UIdelegate = [[VkUIDelegate alloc]init];
    UIdelegate.vc = vc;
    [sdkInstance registerDelegate:delegate];
    [sdkInstance setUiDelegate:UIdelegate];
    
    NSArray *SCOPE = @[@"friends",@"messages",@"docs"];
    [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationInitialized) {
            [VKSdk authorize:SCOPE];
        }
        if (VKAuthorizationError == state) {
            NSLog(@"VKAuthEror");
        }
        if (state == VKAuthorizationAuthorized) {
            NSLog(@"VKAuthYES");
        } else if (error) {
            
        }
    }];

}
+(void)getUserInfo{
    VKRequest* audioReq = [VKApi requestWithMethod:@"account.getProfileInfo" andParameters:@{}];
    [audioReq executeWithResultBlock:
     ^(VKResponse * response){
         
         NSDictionary *dict = [VkAuth ResponseToDict:response];
         NSString *first_name = [dict objectForKey:@"first_name"];
         NSString *last_name = [dict objectForKey:@"last_name"];
         
        
         [[NSNotificationCenter defaultCenter]postNotificationName:@"User Info did Downloaded" object:nil userInfo:@{@"first_name":first_name,@"last_name":last_name}];
         
     } errorBlock:^(NSError * error) {
         if (error.code != VK_API_ERROR) {
             [error.vkError.request repeat];
         }
         else {
             NSLog(@"VK error: %@", error);
         }
     }];

}
+ (NSDictionary *)ResponseToDict:(VKResponse *)response{
    //NSLog(@"Json result: %@", response.json);
    NSError *error;
    NSData *data = [NSJSONSerialization dataWithJSONObject:response.json
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:&error];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&error];
    return dict;
}
@end
