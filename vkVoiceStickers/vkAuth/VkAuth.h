//
//  VkAuth.h
//  test2
//
//  Created by Artem Zabludovsky on 11.04.17.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VKSdk.h>

@interface VkAuth : NSObject

- (void)autentification: (UIViewController *) vc;

+ (void)getUserInfo;// RETURN NOTIFICATION @"User Info did Downloaded" NSDictionary *dict = [notif userInfo]; UserInfo=dict;

+ (NSDictionary *)ResponseToDict:(VKResponse *)response;

@end
