//
//  AlbumbsTableViewCell.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "AlbumbsTableViewCell.h"
#import <Masonry.h>

@implementation AlbumbsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _nameLabel = [UILabel new];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont systemFontOfSize:23 weight:UIFontWeightHeavy];
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView).with.offset(10);
            make.width.equalTo(self.contentView);
            make.height.equalTo(@60);
        }];
    }
    return self;
}


@end
