//
//  LockedAlbumsTableViewCell.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 09.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "LockedAlbumsTableViewCell.h"
#import "UIImage+StickersSize.h"
#import <Masonry.h>

@implementation LockedAlbumsTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _viewForLockAlbum = [UIView new];
        UIView *darkBlur = [UIView new];
        darkBlur.backgroundColor = [UIColor blackColor];
        darkBlur.alpha = 0.5;
        [_viewForLockAlbum addSubview:darkBlur];
        
        UIImage *zamok = [UIImage imageNamed:@"zamok"];
        UIImageView *zamokView = [[UIImageView alloc]initWithImage:[UIImage imageWithImage:zamok forSize:CGSizeMake(70, 70)]];
        [_viewForLockAlbum addSubview:zamokView];
        [self.contentView addSubview:_viewForLockAlbum];
        
        [_viewForLockAlbum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.contentView);
        }];
        [zamokView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_viewForLockAlbum);
        }];
        [darkBlur mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(self.contentView);
            make.center.equalTo(_viewForLockAlbum);
        }];
    }
    return self;
}

@end
