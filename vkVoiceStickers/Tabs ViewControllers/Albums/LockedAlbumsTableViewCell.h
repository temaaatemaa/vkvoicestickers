//
//  LockedAlbumsTableViewCell.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 09.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "AlbumbsTableViewCell.h"

@interface LockedAlbumsTableViewCell : AlbumbsTableViewCell

@property (assign, nonatomic) BOOL isLocked;
@property (strong, nonatomic) UIView *viewForLockAlbum;

@end
