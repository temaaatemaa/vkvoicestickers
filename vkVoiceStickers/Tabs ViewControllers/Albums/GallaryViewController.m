//
//  ViewController.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "GallaryViewController.h"
#import "VkAuth.h"
#import <VKSdk.h>
#import "VKVoiceMessageSend.h"
#import <Masonry.h>
#import "GallaryOfVoiceMessagesViewController.h"
#import "AlbumbsTableViewCell.h"
#import "YandexReport.h"
#import "UIColor+StickersColor.h"
#import "DataAboutStickersFiles.h"
#import "PurchaseMaster.h"
#import "LockedAlbumsTableViewCell.h"
#import "UIImage+StickersSize.h"
#import "SearchStickersViewController.h"


@interface GallaryViewController ()<UITableViewDelegate,UITableViewDataSource,DataAboutStickersDelegate>


@property (nonatomic, strong) VKVoiceMessageSend *send;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic,strong) NSDictionary *dataAboutStickers;

@property (nonatomic, strong) NSArray *arrayOfPersons;

@property (nonatomic,strong)DataAboutStickersFiles *dataAboutStickersExample;

@property (nonatomic,assign)BOOL isVIP;
@property (nonatomic,assign)BOOL isTableForVIP;

@property (nonatomic,copy)NSString *keyForDataForStickers;


@end


@implementation GallaryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.isVIP)
    {
        _isTableForVIP = YES;
    }
    
    [self setupStatusBarBackground];
    
    [self alertToLoginVKInFirstStart];
    [self authButtonClicked];
    
    [self initStickersDataAndArrayOfPersons];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [self setupTableView];

    [self initSearchButton];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    if ((self.isVIP)&&(!self.isTableForVIP))
    {
        [self.tableView reloadData];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Custom Accessors

-(BOOL)isVIP
{
    return [PurchaseMaster stickersIsBuy];
}

- (NSString *)keyForDataForStickers
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    
    return [[version stringByAppendingString:@"--"] stringByAppendingString:build];
}

#pragma mark - Private Methods

- (void)setupStatusBarBackground
{
    CGRect rectForStatusBar = [UIApplication sharedApplication].statusBarFrame;
    UIView *statusBG = [[UIView alloc] initWithFrame:
                        CGRectMake(0, 0, CGRectGetWidth(self.view.frame),CGRectGetHeight(rectForStatusBar))];
    [self.view addSubview:statusBG];
    [statusBG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.height.equalTo(@(CGRectGetHeight(rectForStatusBar)));
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    statusBG.backgroundColor = [UIColor colorForHeaderInSettings];
    
}

- (void)alertToLoginVKInFirstStart
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    if (![userdef objectForKey:@"isFirstTime"]) {
        NSString *title = NSLocalizedString(@"Account VK","");
        NSString *message = NSLocalizedString(@"You need log in to VK to send voice stickers from you. Access to documents is needed for save voice messages. Access to friends need to choose dialog and send message.", "");
        NSString *ok = NSLocalizedString(@"OK","");
        NSString *cancel = NSLocalizedString(@"Cancel","");
        
        UIAlertController *al = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self authButtonClicked];
            [YandexReport reportEventAuth];
        }];
        UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [YandexReport reportEventCancelAuth];
        }];
        [al addAction:cancelButton];
        [al addAction:okButton];
        
        [self presentViewController:al animated:YES completion:nil];
        [userdef setObject:@"no" forKey:@"isFirstTime"];
    }
}

- (void)initStickersDataAndArrayOfPersons
{
    self.dataAboutStickers = [DataAboutStickersFiles getDataAboutStickersFiles];
    
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    if ([userDef objectForKey:self.keyForDataForStickers])
    {
        self.arrayOfPersons = [NSKeyedUnarchiver unarchiveObjectWithData:[userDef objectForKey:self.keyForDataForStickers]];
        return;
    }
    _dataAboutStickersExample = [DataAboutStickersFiles new];
    self.dataAboutStickersExample.delegate = self;
    [self.dataAboutStickersExample getArrayOfPersonsFromInternet];
}

- (void)setupTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectNull style:UITableViewStylePlain];
    [_tableView registerClass:[AlbumbsTableViewCell class] forCellReuseIdentifier:@"cell2"];
    [_tableView registerClass:[LockedAlbumsTableViewCell class] forCellReuseIdentifier:@"cell3"];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorForHeaderInSettings];
    
    [self.view addSubview:_tableView];
    
    CGRect rectForStatusBar = [UIApplication sharedApplication].statusBarFrame;
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(rectForStatusBar.size.height);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
}

- (void)initSearchButton
{
    CGFloat sizeForButton = self.view.frame.size.width*0.19;
    UIButton *searchButton = [UIButton new];
    [searchButton addTarget:self action:@selector(goToSearch) forControlEvents:UIControlEventTouchDown];
    searchButton.backgroundColor = [UIColor colorForSearchStickersButton];
    searchButton.layer.cornerRadius = sizeForButton/2;
    [searchButton setImage:[UIImage imageWithImage:[UIImage imageNamed:@"whiteSearch"] forSize:CGSizeMake(sizeForButton*2/3, sizeForButton*2/3)] forState:UIControlStateNormal];
    
    [self.view addSubview:searchButton];
    
    [searchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(sizeForButton));
        make.width.equalTo(@(sizeForButton));
        make.top.equalTo(self.tableView).with.offset(10);
        make.right.equalTo(self.tableView).with.offset(-10);
    }];
}

- (void)showAlertToBuyVIP
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Альбом не доступен!" message:@"Для доступу к альбому требуется VIP статус. Купить VIP статус можно в настройках." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okButton];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

#pragma mark - Buttons

-(void)authButtonClicked
{
    VkAuth *auth = [[VkAuth alloc]init];
    [auth autentification:self];
}

-(void)unauthButton
{
    [VKSdk forceLogout];
}

- (void)goToSearch
{
    SearchStickersViewController *searchVC = [SearchStickersViewController new];
    searchVC.arrayOfPersons = self.arrayOfPersons;
    [self.navigationController showViewController:searchVC sender:self];
}

#pragma mark - TableViewDelegate

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    AlbumbsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell2"];
    if (![self.arrayOfPersons[0] isEqualToString:@"VoiceStickersPack1"])//Для об мана ревью
    {
        cell.nameLabel.text = self.arrayOfPersons[indexPath.row];
    }
    
    NSNumber *soundAndBannerNumber = self.dataAboutStickers[self.arrayOfPersons[indexPath.row]][@"soundNumber"];
    UIImage *picture = [UIImage imageNamed:[NSString stringWithFormat:@"img_%ld",(long)soundAndBannerNumber.integerValue]];
    cell.backgroundView = [[UIImageView alloc]initWithImage:picture];
    cell.selectedBackgroundView = [[UIImageView alloc]initWithImage:picture];

    
    if ([self.dataAboutStickers[self.arrayOfPersons[indexPath.row]][@"paid"] isEqualToString:@"YES"])
    {
        if (!self.isVIP)
        {
            LockedAlbumsTableViewCell *lockedCell = [tableView dequeueReusableCellWithIdentifier:@"cell3"];
            lockedCell.backgroundView = cell.backgroundView;
            lockedCell.nameLabel.text = cell.nameLabel.text;
            lockedCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return lockedCell;
        }
    }
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrayOfPersons count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat heightForBanner = self.view.frame.size.height * 0.25;
    return heightForBanner;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.dataAboutStickers[self.arrayOfPersons[indexPath.row]][@"paid"] isEqualToString:@"YES"])
    {
        if (!self.isVIP)
        {
            [YandexReport reportEventWithName:@"Not vip acceess" parameters:@{@"albumName":self.arrayOfPersons[indexPath.row]}];
            [self showAlertToBuyVIP];
            return;
        }
    }
    GallaryOfVoiceMessagesViewController *gallary = [GallaryOfVoiceMessagesViewController new];
    gallary.albumName = self.arrayOfPersons[indexPath.row];
    gallary.dictionaryOfMessageCount = self.dataAboutStickers;
    [self.navigationController pushViewController:gallary animated:YES];
    [YandexReport reportEventWithName:@"Choose album" parameters:@{@"albumName":self.arrayOfPersons[indexPath.row]}];
}


#pragma mark - DataAboutStickersDelegate

- (void)arrayOfPersonsHasBeenReceived:(DataAboutStickersFiles *)dataAboutStickers withData:(NSDictionary *)dict
{
    self.arrayOfPersons = dict[@"arrayOfPersons"];
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    NSData *archive = [NSKeyedArchiver archivedDataWithRootObject:self.arrayOfPersons];
    [userDef setObject:archive forKey:self.keyForDataForStickers];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

@end
