//
//  AlbumbsTableViewCell.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumbsTableViewCell : UITableViewCell

@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UIImageView *pictureImageView;
@property (strong, nonatomic) UIImage *pictureImage;



@end
