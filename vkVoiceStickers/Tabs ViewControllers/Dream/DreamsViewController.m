//
//  DreamsViewController.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 17.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "DreamsViewController.h"
#import "UIColor+StickersColor.h"
#import "YandexReport.h"

@interface DreamsViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UILabel *confirmLabel;

@end

@implementation DreamsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _sendButton.backgroundColor = [UIColor colorForHeaderInSettings];
    
    _textField.clearsOnBeginEditing = YES;
    
    self.view.backgroundColor = [UIColor mainColor];
    _confirmLabel.alpha = 0.01;
    _sendButton.layer.cornerRadius = 5;
    [_sendButton addTarget:self action:@selector(sendButtonClicked) forControlEvents:UIControlEventTouchDown];
    [YandexReport reportEventWithName:@"Enter to dreams" parameters:@{}];
}


- (void)sendButtonClicked
{
    NSString *dream = _textField.text;
    if (!dream)
    {
        return;
    }
    [YandexReport reportEventWithName:@"Dream" parameters:@{@"want":dream}];
    _textField.text = @"";
    [UIView animateWithDuration:0.3 animations:^{
        _confirmLabel.alpha = 1;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 delay:1 options:0 animations:^{
            _confirmLabel.alpha = 0.01;
        } completion:^(BOOL finished) {

        }];
    }];
    [self.view endEditing:YES];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


@end
