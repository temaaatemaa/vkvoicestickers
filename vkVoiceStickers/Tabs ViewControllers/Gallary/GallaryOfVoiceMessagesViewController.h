//
//  GallaryOfVoiceMessagesViewController.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GallaryOfVoiceMessagesViewController : UIViewController


@property (nonatomic, copy) NSString *albumName;
@property (nonatomic, strong) NSDictionary *dictionaryOfMessageCount;


@end
