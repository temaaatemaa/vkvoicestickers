//
//  GallaryOfVoiceMessagesViewController.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "GallaryOfVoiceMessagesViewController.h"
#import <Masonry.h>
#import "GallaryCollectionViewCell.h"
#import "PlayVoiceSticker.h"
#import "ChooseDialogViewController.h"
#import "YandexReport.h"
#import <VKSdk.h>
#import "VkAuth.h"
#import "DataAboutStickersFiles.h"
#import "UIColor+StickersColor.h"
#import <EAIntroView/EAIntroView.h>


@interface GallaryOfVoiceMessagesViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>


@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIImageView *bannerImageView;
@property (nonatomic, strong) UIView *viewForButton;
@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, strong) UIButton *sendTelegramButton;
@property (nonatomic, strong) UIView *sendView;
@property (nonatomic, strong) UIView *viewForSoundButton;
@property (nonatomic, strong) UIButton *soundButton;
@property (nonatomic, strong) UIView  *waitingIndicatorView;
@property (nonatomic, strong) UIActivityIndicatorView  *waitingIndicator;

@property (nonatomic, assign) NSNumber *soundNumber;

@property (nonatomic, assign) BOOL isSound;
@property (nonatomic, strong) PlayVoiceSticker *player;


@end


@implementation GallaryOfVoiceMessagesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    
    self.view.backgroundColor = [UIColor backgroundColorOfGallary];
    
    [self initBannerView];
    
    [self initCollectionView];
    
    [self initSoundView];
    
    [self initSoundButton];
    
    [self updateViewConstraints];
    
    [self setNeedsStatusBarAppearanceUpdate];
    [self.view layoutIfNeeded];
}


#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.view layoutIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //[self showIntroView]; ИНТРО ВЬЮ ВКЛЮЧИТЬ КОГДА НУЖНО
}


#pragma mark - initialize UI


- (void)initSendView
{
    if (self.sendView) {
        return;
    }
    _sendView = [[UIView alloc]init];
    [self.view addSubview:_sendView];
}
- (void)initSendButtonAndSendTelegramButton
{
    if (self.sendButton) {
        return;
    }
    _sendButton = [[UIButton alloc]init];
    _sendButton.backgroundColor = [UIColor colorForSendButton];
    _sendButton.layer.cornerRadius = 10;
    [_sendButton setTitle:NSLocalizedString(@"Send VK","") forState:UIControlStateNormal];
    [_sendButton addTarget:self action:@selector(sendSticker) forControlEvents:UIControlEventTouchDown];
    [self.sendView addSubview:_sendButton];
    
    _sendTelegramButton = [[UIButton alloc]init];
    _sendTelegramButton.backgroundColor = [UIColor colorForSendButton];
    _sendTelegramButton.layer.cornerRadius = 10;
    [_sendTelegramButton setTitle:@"Telegram/WhatsApp" forState:UIControlStateNormal];
    if (self.view.frame.size.width<350) {
        [_sendTelegramButton setTitle:@"Telegram/WApp" forState:UIControlStateNormal];
    }
    [_sendTelegramButton addTarget:self action:@selector(sendTelegramSticker) forControlEvents:UIControlEventTouchDown];
    [self.sendView addSubview:_sendTelegramButton];
    
    _sendTelegramButton.titleLabel.numberOfLines = 2;
    
    _sendView.translatesAutoresizingMaskIntoConstraints = NO;
    _sendTelegramButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_sendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sendView);
        make.right.equalTo(_sendTelegramButton.mas_left).with.offset(-7);
        make.top.equalTo(self.sendView);
        make.bottom.equalTo(self.sendView);
    }];
    [_sendTelegramButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_sendButton);
        make.right.equalTo(self.sendView);
        make.top.equalTo(self.sendView);
        make.bottom.equalTo(self.sendView);
    }];
}

- (void)initCollectionView
{
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectNull collectionViewLayout:layout];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    [_collectionView registerClass:[GallaryCollectionViewCell class] forCellWithReuseIdentifier:@"cell1"];
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
    _collectionView.backgroundColor = [UIColor backgroundColorOfCollectionView];
    [self.view addSubview:_collectionView];
}

- (void)initBannerView
{
    _bannerImageView = [[UIImageView alloc]initWithFrame:CGRectNull];
    [_bannerImageView setUserInteractionEnabled:YES];
    
    
    _bannerImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"img_%ld",(long)self.soundNumber.integerValue]];
    [self.view addSubview:_bannerImageView];
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    [_soundButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(_viewForSoundButton);
        make.center.equalTo(_viewForSoundButton);
    }];
    
    [_viewForSoundButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(_viewForSoundButton.mas_width);
        make.width.equalTo(@(self.view.frame.size.width*0.15));
        make.bottom.equalTo(_bannerImageView).with.offset(-5);
        make.right.equalTo(_bannerImageView).with.offset(-5);
    }];
    CGFloat heightForBanner = self.view.frame.size.height*0.3;
    [_bannerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(@(heightForBanner));
    }];
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view);
        make.top.equalTo(_bannerImageView.mas_bottom).offset(5);
        make.left.equalTo(self.view).offset(5);
        make.right.equalTo(self.view).offset(-5);
    }];
}
- (void)initSoundView
{
    _viewForSoundButton = [[UIView alloc]init];
    _viewForSoundButton.layer.cornerRadius = self.view.frame.size.width*0.15/2;
    _viewForSoundButton.backgroundColor = [UIColor colorSoundButton];
    [_bannerImageView addSubview:_viewForSoundButton];
}
- (void)initSoundButton
{
    _soundButton = [UIButton new];
    [_soundButton setBackgroundImage:[UIImage imageNamed:@"Sound"] forState:UIControlStateNormal];
    _isSound = YES;
    [_soundButton addTarget:self action:@selector(changeSound) forControlEvents:UIControlEventTouchDown];
    [_bannerImageView addSubview:_soundButton];
}

- (void)setupNavigationBar
{
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc]init];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.hidden = NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Custom Accessors

-(NSNumber *)soundNumber
{
    NSDictionary *dictOfStickers = self.dictionaryOfMessageCount;
    NSNumber *soundAndBannerNumber = dictOfStickers[self.albumName][@"soundNumber"];
    return soundAndBannerNumber;
}
#pragma mark - Actions

- (void)changeSound
{
    if (self.isSound)
    {
        [self.soundButton setBackgroundImage:[UIImage imageNamed:@"noSound"] forState:UIControlStateNormal];
        self.isSound = NO;
        self.player = nil;
    }
    else
    {
        [self.soundButton setBackgroundImage:[UIImage imageNamed:@"Sound"] forState:UIControlStateNormal];
        self.isSound = YES;
    }
}

- (void)sendSticker
{
    if (![VKSdk accessToken].accessToken)
    {
        [self showAlertWhenUserIsNotLogin];
    }
    
    NSIndexPath *selectedItem = self.collectionView.indexPathsForSelectedItems.firstObject;
    
    ChooseDialogViewController *choose = [ChooseDialogViewController new];
    
    //for testing albums like vkvoicestickerpack1 -2 -3
    if ([self.dictionaryOfMessageCount[self.albumName][@"test"] isEqualToString:@"YES"])
    {
        NSNumber *soundNumber = self.dictionaryOfMessageCount[self.albumName][@"stickers"][selectedItem.row][@"soundNumber"];
        NSNumber *stickerNumber = self.dictionaryOfMessageCount[self.albumName][@"stickers"][selectedItem.row][@"stickersNumber"];
        choose.nameOfSound = [NSString stringWithFormat:@"%ld_sound_%ld",(long)soundNumber.integerValue,(long)stickerNumber.integerValue];
        [self presentViewController:choose animated:YES completion:nil];
        [YandexReport reportEventWithName:@"VK TEST Send sticker"
                               parameters:@{
                                            @"albumName":self.albumName,
                                            @"sticker":self.dictionaryOfMessageCount[self.albumName][@"stickers"][selectedItem.row]
                                            }];
        return;
    }
    
    choose.nameOfSound = [NSString stringWithFormat:@"%ld_sound_%ld",(long)self.soundNumber.integerValue,(long)selectedItem.row];
    [self presentViewController:choose animated:YES completion:nil];
    
    
    [YandexReport reportEventWithName:@"Send sticker"
                           parameters:@{
                                        @"albumName":self.albumName,
                                        @"sticker":self.dictionaryOfMessageCount[self.albumName][@"stickers"][selectedItem.row]
                                        }];
    [YandexReport reportEventWithName:@"Send sticker via VK"
                           parameters:@{
                                        @"albumName":self.albumName,
                                        @"sticker":self.dictionaryOfMessageCount[self.albumName][@"stickers"][selectedItem.row]
                                        }];
}

- (void)sendTelegramSticker
{
    NSIndexPath *selectedItem = self.collectionView.indexPathsForSelectedItems.firstObject;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSURL *fileURL;
        
        //for testing albums like vkvoicestickerpack1 -2 -3
        if ([self.dictionaryOfMessageCount[self.albumName][@"test"] isEqualToString:@"YES"])
        {
            NSNumber *soundNumber = self.dictionaryOfMessageCount[self.albumName][@"stickers"][selectedItem.row][@"soundNumber"];
            NSNumber *stickerNumber = self.dictionaryOfMessageCount[self.albumName][@"stickers"][selectedItem.row][@"stickersNumber"];
            fileURL = [[NSBundle mainBundle]
                       URLForResource:[NSString stringWithFormat:@"%ld_sound_%ld",(long)soundNumber.integerValue,(long)stickerNumber.integerValue] withExtension:@"mp3"];
        }
        else
        {
            fileURL = [[NSBundle mainBundle]
                       URLForResource:[NSString stringWithFormat:@"%ld_sound_%ld",(long)self.soundNumber.integerValue,(long)selectedItem.row] withExtension:@"mp3"];
        }

        if (!fileURL)
        {
            return ;
        }
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:@[fileURL] applicationActivities:nil];
        activityViewController.excludedActivityTypes = @[UIActivityTypeMail,UIActivityTypeAirDrop, UIActivityTypeMessage,UIActivityTypeCopyToPasteboard];
        activityViewController.popoverPresentationController.sourceView = self.view;
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self presentViewController:activityViewController animated:YES completion:^{
                NSLog(@"open");
            }];
        });
    });
    
    [YandexReport reportEventWithName:@"Send sticker"
                           parameters:@{
                                        @"albumName":self.albumName,
                                        @"sticker":self.dictionaryOfMessageCount[self.albumName][@"stickers"][selectedItem.row]
                                        }];
    
    [YandexReport reportEventWithName:@"Send sticker not vk"
                           parameters:@{
                                        @"albumName":self.albumName,
                                        @"sticker":self.dictionaryOfMessageCount[self.albumName][@"stickers"][selectedItem.row]
                                        }];
}

-(void)showSendButton
{
    [self initSendView];
    [self initSendButtonAndSendTelegramButton];
    [self.view layoutIfNeeded];
    
    [self animationForSendView];
}

#pragma mark - Public
#pragma mark - Private

- (void)animationForSendView
{
    _sendView.frame = CGRectMake(10, self.view.frame.size.height, self.view.frame.size.width-20, 50);
    [self.view layoutIfNeeded];//for buttons
    
    CGRect rectForTabBar = self.tabBarController.tabBar.frame;
    [_sendView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).with.offset(-5-rectForTabBar.size.height);
        make.centerX.equalTo(self.view);
        make.height.equalTo(@(self.view.frame.size.height/15));
        make.width.equalTo(self.view).with.offset(-20);
    }];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
        [self.view setNeedsLayout];
    }];
}

- (void)showAlertWhenUserIsNotLogin
{
    NSString *title = NSLocalizedString(@"Account VK","");
    NSString *message = NSLocalizedString(@"You need log in to VK to send voice stickers from you. Access to documents is needed for save voice messages. Access to friends need to choose dialog and send message.", "");
    NSString *ok = NSLocalizedString(@"OK","");
    NSString *cancel = NSLocalizedString(@"Cancel","");
    
    UIAlertController *al = [UIAlertController alertControllerWithTitle:title
                                                                message:message
                                                         preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        VkAuth *auth = [[VkAuth alloc]init];
        [auth autentification:self];
        [YandexReport reportEventAuth];
        if ([VKSdk isLoggedIn])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self sendSticker];
            });
        }
    }];
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [YandexReport reportEventCancelAuth];
    }];
    [al addAction:cancelButton];
    [al addAction:okButton];
    
    [self presentViewController:al animated:YES completion:^{
        if ([VKSdk isLoggedIn])//????????????
        {
            [self sendSticker];
        }
    }];
}

- (void)showIntroView
{
        NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
        if (![userdef objectForKey:@"notNeesToShowIntro"])
        {
            EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:[self setupIntroPages]];
            [intro showFullscreen];
            [userdef setObject:@"YES" forKey:@"notNeesToShowIntro"];
        }
}

- (NSArray *)setupIntroPages
{
    // basic
    EAIntroPage *page1 = [EAIntroPage page];
    page1.title = @"Telegram";
    page1.titleFont = [UIFont systemFontOfSize:25 weight:UIFontWeightThin];
    page1.titlePositionY = self.view.frame.size.height*0.4;
    page1.desc = NSLocalizedString(@"We present you new functionality! \n Now you can send stickers to Telegram", @"");
    page1.descFont = [UIFont systemFontOfSize:17 weight:UIFontWeightThin];
    page1.descPositionY = 200;//self.view.frame.size.height*0.9;
    page1.bgColor = [UIColor mainColor];
    page1.titleIconView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"telegram"]];
    //page1.titleIconPositionY = -50;
    
    
    // custom
    EAIntroPage *page2 = [EAIntroPage page];
    page2.bgColor = [UIColor mainColor];
    page2.title = @"WhatsApp";
    page2.titleFont = [UIFont systemFontOfSize:25 weight:UIFontWeightThin];
    page2.titlePositionY = self.view.frame.size.height*0.4;
    page2.desc = NSLocalizedString(@"And else... \n You can send stickers to WhatsApp!", @"");
    page2.descFont = [UIFont systemFontOfSize:17 weight:UIFontWeightThin];
    page2.descPositionY = 200;
    page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"whatsapp"]];
    
    EAIntroPage *page3 = [EAIntroPage page];
    page3.bgColor = [UIColor mainColor];
    page3.title = @"Афоня";
    page3.titleFont = [UIFont systemFontOfSize:25 weight:UIFontWeightThin];
    page3.titlePositionY = self.view.frame.size.height*0.4;
    page3.desc = NSLocalizedString(@"And there is one more news! \n Add new stickers :)\n And it is begin, you wil see new updates soon", @"");
    page3.descFont = [UIFont systemFontOfSize:17 weight:UIFontWeightThin];
    page3.descPositionY = 200;
    page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_12"]];
    page3.titleIconPositionY = self.view.frame.size.height*0.2;

    return @[page1,page2,page3];
}


#pragma mark - UICollectionViewDelegate

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
     GallaryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell1" forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorForCollectionViewCell];
    cell.contentView.layer.cornerRadius = 3;
    //selection
    NSIndexPath *selected = collectionView.indexPathsForSelectedItems.firstObject;
    if ([selected isEqual:indexPath])
    {
        [cell performSelectionAnimations];
    }
    else
    {
        [cell performUnselectionAnimations];
    }
    
    //for testing albums
    if ([self.dictionaryOfMessageCount[self.albumName][@"test"] isEqualToString:@"YES"])
    {
        NSLog(@"start %ld",(long)indexPath.row);
        cell.nameSoundLabel.text = self.dictionaryOfMessageCount[self.albumName][@"stickers"][indexPath.row][@"stickerName"];
        NSLog(@"end %ld",(long)indexPath.row);
        return cell;
    }
    
    cell.nameSoundLabel.text = self.dictionaryOfMessageCount[self.albumName][@"stickers"][indexPath.row];
    
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return ((NSNumber *)(self.dictionaryOfMessageCount[self.albumName][@"count"])).integerValue;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.view.frame.size.width;
    
    return CGSizeMake(width/4-4, width/4-4);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    GallaryCollectionViewCell *cell = (GallaryCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell performSelectionAnimations];
    [self showSendButton];
    
    if (_isSound)
    {
        _player = [PlayVoiceSticker new];
        //for testing albums
        if ([self.dictionaryOfMessageCount[self.albumName][@"test"] isEqualToString:@"YES"])
        {
            NSNumber *soundNumber = self.dictionaryOfMessageCount[self.albumName][@"stickers"][indexPath.row][@"soundNumber"];
            NSNumber *stickerNumber = self.dictionaryOfMessageCount[self.albumName][@"stickers"][indexPath.row][@"stickersNumber"];
            [_player playStickerWithName:[NSString stringWithFormat:@"%ld_sound_%ld",(long)soundNumber.integerValue,(long)stickerNumber.integerValue]];
        }
        else
        {
            [_player playStickerWithName:[NSString stringWithFormat:@"%ld_sound_%ld",(long)self.soundNumber.integerValue,(long)indexPath.row]];
        }
    }
    
    
    [YandexReport reportEventWithName:@"Play sticker"
                           parameters:@{
                                        @"albumName":self.albumName,
                                        @"sticker":self.dictionaryOfMessageCount[self.albumName][@"stickers"][indexPath.row]
                                        }];
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GallaryCollectionViewCell *selectedCell = (GallaryCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [selectedCell performUnselectionAnimations];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake(self.view.frame.size.width,self.view.frame.size.height/15+10);
}



@end
