//
//  GallaryCollectionViewCell.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GallaryCollectionViewCell : UICollectionViewCell


@property (nonatomic, strong)UILabel *nameSoundLabel;

- (void)performSelectionAnimations;
- (void)performUnselectionAnimations;

@end
