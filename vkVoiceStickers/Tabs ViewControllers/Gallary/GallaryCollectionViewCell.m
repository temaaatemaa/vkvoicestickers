//
//  GallaryCollectionViewCell.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "GallaryCollectionViewCell.h"
#import <Masonry.h>
#import "UIColor+StickersColor.h"


@implementation GallaryCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _nameSoundLabel = [UILabel new];
        _nameSoundLabel.textColor = [UIColor blackColor];
        _nameSoundLabel.numberOfLines = 4;
        _nameSoundLabel.textAlignment = NSTextAlignmentCenter;
        _nameSoundLabel.lineBreakMode = NSLineBreakByClipping;
        [self.contentView addSubview:_nameSoundLabel];
        
        [_nameSoundLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.contentView);
            make.height.equalTo(self.contentView);
            make.width.equalTo(self.contentView);
        }];
        if (self.isSelected)
        {
            [self performSelectionAnimations];
        }
        else
        {
            [self performUnselectionAnimations];
        }
    }
    return self;
}

- (void)performSelectionAnimations
{
    self.contentView.backgroundColor = [UIColor colorForSendButton];
    _nameSoundLabel.textColor = [UIColor whiteColor];
}

- (void)performUnselectionAnimations
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    _nameSoundLabel.textColor = [UIColor blackColor];
}
@end
