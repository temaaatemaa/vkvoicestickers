//
//  ChooseDialogViewController.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseDialogViewController : UIViewController

@property (strong, nonatomic) NSString *nameOfSound;

@end
