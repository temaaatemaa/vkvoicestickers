//
//  ChooseDialogViewController.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "ChooseDialogViewController.h"
#import "DialogsAndFriendsDownload.h"
#import <Masonry.h>
#import "VkAuth.h"
#import "VKVoiceMessageSend.h"
#import "YandexReport.h"
#import "UIColor+StickersColor.h"
#import "UIImage+StickersSize.h"

@interface ChooseDialogViewController ()<DialogsAndFriendsDownloadDelegate, UITableViewDelegate, UITableViewDataSource, VKVoiceMessageSendDelegate, UISearchBarDelegate>
@property (strong, nonatomic) NSArray *arrOfDialogs;
@property (strong, nonatomic) NSArray *arrOfUsers;
@property (strong, nonatomic) NSArray *arrOfAllUsersForSearchBar;
@property (strong, nonatomic) NSDictionary *dictOfFotos;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) DialogsAndFriendsDownload *download;
@property (strong, nonatomic) UIView *viewForSendingMessageText;

@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) UIButton *dialogsButton;
@property (strong, nonatomic) UIButton *friendsButton;
@property (strong, nonatomic) UIButton *backButton;

@property (strong, nonatomic) UILabel *labelForTitle;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (assign, nonatomic) BOOL isDialogsCountGreaterThen0;
@end

@implementation ChooseDialogViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    _download = [DialogsAndFriendsDownload new];
    _download.delegate = self;
    
    if (![VKSdk accessToken].accessToken){
        VkAuth *auth = [[VkAuth alloc]init];
        [auth autentification:self];
    }
    
    _isDialogsCountGreaterThen0 = YES;
    [_download downloadDialogsWithOffset:0];
    
    
    _headerView = [self setupHeaderView];
    
    [self setupTableView];
    
    _backButton = [self setupBackButton];
    
    [self setupDialogsButton];
    [self setupFriendsButton];
    
    [self updateViewConstraints];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

#pragma mark - UI

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)updateViewConstraints
{
    [_friendsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headerView.mas_bottom);
        make.right.equalTo(self.view);
        make.height.equalTo(@(self.view.frame.size.height*0.05));
    }];
    
    [_dialogsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headerView.mas_bottom);
        make.left.equalTo(self.view);
        make.width.equalTo(_friendsButton);
        make.right.equalTo(_friendsButton.mas_left).with.offset(-1);
        make.height.equalTo(_friendsButton);
    }];
    
    [_labelForTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(_headerView).with.offset(5);
        make.left.equalTo(_headerView);
        make.right.equalTo(_headerView);
    }];
    
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.height.equalTo(@(self.view.frame.size.height*0.13));
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_friendsButton.mas_bottom);
        make.bottom.equalTo(_backButton.mas_top);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    
    [_backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view);
        make.height.equalTo(@40);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
}

- (UIView *)setupHeaderView
{
    UIView *header = [[UIView alloc]initWithFrame:CGRectNull];
    header.backgroundColor = [UIColor colorForHeaderInDialogs];
    [self.view addSubview:header];
    _labelForTitle = [[UILabel alloc]init];
    _labelForTitle.text = NSLocalizedString(@"Choose dialog", "");
    _labelForTitle.textAlignment = NSTextAlignmentCenter;
    _labelForTitle.textColor = [UIColor whiteColor];
    _labelForTitle.font = [UIFont systemFontOfSize:23 weight:UIFontWeightMedium];
    [header addSubview:_labelForTitle];
    
    return header;
}

- (void)setupTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectNull style:UITableViewStylePlain];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
}

- (UIButton *)setupBackButton
{
    UIButton *backButton = [[UIButton alloc]init];
    [backButton addTarget:self action:@selector(closeVC) forControlEvents:UIControlEventTouchDown];
    backButton.backgroundColor = [UIColor colorForCancelButtonInDialogs];
    [backButton setTitle:@"X" forState:UIControlStateNormal];
    [self.view addSubview:backButton];
    return backButton;
}

- (void)setupDialogsButton
{
    _dialogsButton = [[UIButton alloc]init];
    [_dialogsButton setTitle:NSLocalizedString(@"Dialogs", "") forState:UIControlStateNormal];
    [_dialogsButton addTarget:self action:@selector(dialogsButtonClicked) forControlEvents:UIControlEventTouchDown];
    [_dialogsButton setBackgroundColor:[UIColor colorWithRed:0 green:150.f/255.f blue:215.f/255.f alpha:0.8]];
    [self.view addSubview:_dialogsButton];
}

- (void)setupFriendsButton
{
    _friendsButton = [[UIButton alloc]init];
    [_friendsButton setTitle:NSLocalizedString(@"Friends", "") forState:UIControlStateNormal];
    [_friendsButton setBackgroundColor:[UIColor colorForHeaderInDialogs]];
    [_friendsButton addTarget:self action:@selector(friendsButtonClicked) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:_friendsButton];
}

- (void)dialogsButtonClicked
{
    [_dialogsButton setBackgroundColor:[UIColor colorWithRed:0 green:150.f/255.f blue:215.f/255.f alpha:0.8]];
    [_friendsButton setBackgroundColor:[UIColor colorForHeaderInDialogs]];
    _labelForTitle.text = NSLocalizedString(@"Choose dialog", "");
    _arrOfUsers = nil;
    _arrOfDialogs = nil;
    _dictOfFotos = nil;
    
    [_download downloadDialogsWithOffset:0];
    
    [self.view endEditing:YES];
    _searchBar = nil;
    self.tableView.tableHeaderView = nil;
    
    [self.tableView reloadData];
}

#pragma mark - Buttons

- (void)friendsButtonClicked
{
    [_dialogsButton setBackgroundColor:[UIColor colorForHeaderInDialogs]];
    [_friendsButton setBackgroundColor:[UIColor colorWithRed:0 green:150.f/255.f blue:215.f/255.f alpha:0.8]];
    _labelForTitle.text = NSLocalizedString(@"Choose friend", "");
    _arrOfUsers = nil;
    _arrOfDialogs = nil;
    _dictOfFotos = nil;
    [_download downloadFriends];
    
    [self.tableView reloadData];
    
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    _searchBar.delegate = self;
    
    self.tableView.tableHeaderView = _searchBar;
}

#pragma mark - SearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    NSString *text = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSArray *array = [text componentsSeparatedByString:@" "];
    NSString *firstName = text;
    NSString *lastName = text;
    NSPredicate *predicate = nil;
    
    if ([array count] > 1) {
        firstName = array[0];
        lastName = array[1];
        predicate = [NSPredicate predicateWithFormat:@"(first_name CONTAINS[cd] %@ AND last_name CONTAINS[cd] %@) OR (first_name CONTAINS[cd] %@ AND last_name CONTAINS[cd] %@)", firstName, lastName, lastName, firstName];
    } else {
        predicate = [NSPredicate predicateWithFormat:@"first_name CONTAINS[cd] %@ OR last_name CONTAINS[cd] %@", firstName, lastName];
    }
    
    _arrOfUsers = [_arrOfAllUsersForSearchBar filteredArrayUsingPredicate:predicate];
    if ([searchText length] == 0) {
        _arrOfUsers = _arrOfAllUsersForSearchBar;
    }
    [self.tableView reloadData];
}

#pragma mark - DialogsAndFriendsDownloadDelegate

-(void)friends:(NSDictionary *)friends didDownload:(DialogsAndFriendsDownload *)download
{
    _arrOfUsers = friends[@"response"][@"items"];
    _arrOfAllUsersForSearchBar = _arrOfUsers;
    [self downloadFotos];
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dialogs:(NSDictionary *)dialogs didDownload:(DialogsAndFriendsDownload *)dialogsAndFriendsDownload withOffset:(NSInteger)offset {
    NSLog(@"%@",dialogs[@"response"][@"count"]);
    if ([dialogs[@"response"][@"count"] isEqual:@0]) {
        _isDialogsCountGreaterThen0 = NO;
        [self.tableView reloadData];
        return;
    }
    _isDialogsCountGreaterThen0 = YES;
    _arrOfDialogs = dialogs[@"response"][@"items"];
    NSMutableString *stringOfUserId = [[NSMutableString alloc]init];
    NSMutableString *stringOfgroupId = [[NSMutableString alloc]init];
    NSString *tmp;
    for (NSDictionary *dialog in _arrOfDialogs) {
        NSNumber *ids = dialog[@"message"][@"user_id"];
        if (ids.integerValue > 0)
        {
            NSString *str = [NSString stringWithFormat:@"%@",dialog[@"message"][@"user_id"]];
            [stringOfUserId appendString:str];
            [stringOfUserId appendString:@","];
        }
        else
        {
            NSString *str = [NSString stringWithFormat:@"%ld",(long)-ids.integerValue];
            tmp = str;
            [stringOfgroupId appendString:str];
            [stringOfgroupId appendString:@","];
        }
    }
    NSString *str = [NSString stringWithFormat:@"%@",_arrOfDialogs.firstObject[@"message"][@"user_id"]];
    if (str != nil)
    {
        [stringOfUserId appendString:str];
    }
    if (tmp != nil)
    {
        [stringOfgroupId appendString:tmp];
    }
    [self.download downloadInfoAboutUsers:stringOfUserId];
    [self.download downloadInfoAboutGroups:stringOfgroupId];
}

-(void)usersInfo:(NSDictionary *)usersInfo didDownload:(DialogsAndFriendsDownload *)download
{
    if (_arrOfUsers) {
        NSMutableArray *arrayOfUsers = [NSMutableArray arrayWithArray:_arrOfUsers];
        [arrayOfUsers addObjectsFromArray:usersInfo[@"response"]];
        _arrOfUsers = arrayOfUsers;
        [self downloadFotos];
        [self.tableView reloadData];
    }
    else
    {
        _arrOfUsers = usersInfo[@"response"];
        [self downloadFotos];
        [self.tableView reloadData];
    }
}
-(void)groupsInfo:(NSDictionary *)groupsInfo didDownload:(DialogsAndFriendsDownload *)download
{
    NSMutableArray *arrayOfUsers = [NSMutableArray arrayWithArray:_arrOfUsers];
    [arrayOfUsers addObjectsFromArray:groupsInfo[@"response"]];
    _arrOfUsers = arrayOfUsers;
    [self downloadFotos];
    [self.tableView reloadData];
}

#pragma mark - Private Methods

-(void)downloadFotos
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
            for (NSDictionary *user in _arrOfUsers) {
                NSURLSession *session = [NSURLSession sharedSession];
                NSURLSessionDataTask *task = [session dataTaskWithURL:[NSURL URLWithString:user[@"photo_100"]] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        @try {
                            if ((data)&&([UIImage imageWithData:data]))
                            {
                                [dict setObject:[UIImage imageWithData:data] forKey:user[@"id"]];
                            }
                            self.dictOfFotos = dict;
                            [self.tableView reloadData];
                        }
                        @catch (NSException * e) {
                            NSLog(@"Exception: %@", e);
                            [YandexReport reportErrorWithName:@"ERROR: downloadFotos(catch)"];
                        }
                        @finally {
                            
                        }
                    });
                }];
                [task resume];
            }
    
        dispatch_sync(dispatch_get_main_queue(), ^{
            
        self.dictOfFotos = dict;
        [self.tableView reloadData];
        });
    });
    
}

-(void)showLoginView
{
    _viewForSendingMessageText = [UIView new];
    _viewForSendingMessageText.backgroundColor = [UIColor colorForCancelButtonInDialogs];
    //view.layer.cornerRadius = 10;
    
    UILabel *label = [UILabel new];
    label.textColor = [UIColor whiteColor];
    label.text = NSLocalizedString(@"Sending...", "");
    label.textAlignment = NSTextAlignmentCenter;
    
    [_viewForSendingMessageText addSubview:label];
    [self.view addSubview:_viewForSendingMessageText];
    
    [_viewForSendingMessageText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_top);
        make.centerX.equalTo(self.view);
        make.height.equalTo(@(self.view.frame.size.height*0.15));
        make.width.equalTo(self.view);
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(_viewForSendingMessageText);
        make.width.equalTo(_viewForSendingMessageText);
        make.height.equalTo(@50);
    }];
    [self.view layoutIfNeeded];
    [self.view setNeedsLayout];
    
    [UIView animateWithDuration:0.5 animations:^{
        [_viewForSendingMessageText mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.view);
            make.height.equalTo(@(self.view.frame.size.height*0.15));
            make.width.equalTo(self.view);
        }];
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }];
    
}
-(void)closeVC
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [YandexReport reportEventWithName:@"Close choose dialog" parameters:@{}];
    
}


- (void)runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat {
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat ? HUGE_VALF : 0;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}


#pragma mark - VKVoiceMessageSendDelegate

- (void)messageDidSend:(VKVoiceMessageSend *)VKVoiceMessageSend withResponse:(NSDictionary *)response {
    
    [UIView animateWithDuration:0.5 animations:^{
        [_viewForSendingMessageText mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom);
            make.centerX.equalTo(self.view);
            make.height.equalTo(@(self.view.frame.size.height*0.15));
            make.width.equalTo(self.view);
        }];
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }];
    NSString *succses = NSLocalizedString(@"Message has been send", @"");
    NSString *message = NSLocalizedString(@"Message has been send successfully", @"");
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:succses
                                                                             message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okButton];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)messageDidntSend:(VKVoiceMessageSend *)VKVoiceMessageSend withError:(NSError *)error {
    NSString *fail = NSLocalizedString(@"Error!", @"");
    NSString *message = NSLocalizedString(@"Message hasn't been send", @"");
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:fail message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okButton];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}


#pragma mark - TableViewDelegate

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (self.arrOfDialogs == nil) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",self.arrOfUsers[indexPath.row][@"first_name"],self.arrOfUsers[indexPath.row][@"last_name"]];
        UIImage *image = _dictOfFotos[self.arrOfUsers[indexPath.row][@"id"]];
        cell.imageView.image = [UIImage imageWithImage:image forSize:CGSizeMake(60, 60)];
        
        return cell;
    }
    if ([_arrOfDialogs[indexPath.row][@"message"][@"title"] length]) {
        cell.textLabel.text = _arrOfDialogs[indexPath.row][@"message"][@"title"];
        UIImage *image = [UIImage imageNamed:@"dialog"];
        cell.imageView.image = [UIImage imageWithImage:image forSize:CGSizeMake(60, 60)];
    }
    else
    {
        NSNumber *ids = _arrOfDialogs[indexPath.row][@"message"][@"user_id"];
        if (ids.integerValue<0) {
            ids = @(-(ids.integerValue));
            
            NSString *userID = [NSString stringWithFormat:@"%@",ids];
            for (NSDictionary *user in _arrOfUsers) {
                NSString *str = [NSString stringWithFormat:@"%@",user[@"id"]];
                if ([str isEqualToString:userID]) {
                    cell.textLabel.text = [NSString stringWithFormat:@"%@",user[@"name"]];
                    if (_dictOfFotos[user[@"id"]]!=nil) {
                        UIImage *image = _dictOfFotos[user[@"id"]];
                        cell.imageView.image = [UIImage imageWithImage:image forSize:CGSizeMake(60, 60)];
                    }
                    else
                    {
                        UIImage *image = [UIImage imageNamed:@"1"];
                        cell.imageView.image = [UIImage imageWithImage:image forSize:CGSizeMake(60, 60)];
                    }
                    break;
                }
            }
        }
        else
        {
            NSString *userID = [NSString stringWithFormat:@"%@",_arrOfDialogs[indexPath.row][@"message"][@"user_id"]];
            for (NSDictionary *user in _arrOfUsers) {
                NSString *str = [NSString stringWithFormat:@"%@",user[@"id"]];
                if ([str isEqualToString:userID]) {
                    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",user[@"first_name"],user[@"last_name"]];
                    if (_dictOfFotos[user[@"id"]]!=nil) {
                        UIImage *image = _dictOfFotos[user[@"id"]];
                        cell.imageView.image = [UIImage imageWithImage:image forSize:CGSizeMake(60, 60)];
                    }
                    else
                    {
                        UIImage *image = [UIImage imageNamed:@"1"];
                        cell.imageView.image = [UIImage imageWithImage:image forSize:CGSizeMake(60, 60)];
                    }
                    break;
                }
            }
        }
    }
    cell.imageView.layer.cornerRadius = 30;
    cell.imageView.layer.masksToBounds = YES;
    
    
    [cell setNeedsLayout];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_arrOfDialogs == nil) {
        return [_arrOfUsers count];
    }
    return [_arrOfDialogs count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VKVoiceMessageSend *send = [VKVoiceMessageSend new];
    send.delegate = self;
    if (_arrOfDialogs == nil) {
        NSNumber *userid = _arrOfUsers[indexPath.row][@"id"];
        [send sendVoiceMessageWith:self.nameOfSound toUserId:[NSString stringWithFormat:@"%@",userid]];
        [YandexReport reportEventWithName:@"Send to userID" parameters:@{}];
        [self showLoginView];
        return;
    }
    if (_arrOfDialogs[indexPath.row][@"message"][@"chat_id"]) {
        [send sendVoiceMessageWith:self.nameOfSound toChatId:_arrOfDialogs[indexPath.row][@"message"][@"chat_id"]];
        [YandexReport reportEventWithName:@"Send to chatID" parameters:@{}];
    }
    else
    {
        NSNumber *userid = _arrOfDialogs[indexPath.row][@"message"][@"user_id"];
        [send sendVoiceMessageWith:self.nameOfSound toUserId:[NSString stringWithFormat:@"%@",userid]];
        [YandexReport reportEventWithName:@"Send to userID" parameters:@{}];
    }
    [self showLoginView];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (([self.arrOfDialogs count])||([self.arrOfUsers count]))
    {
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
        return 1;
    }
    else
    {
        if ((_isDialogsCountGreaterThen0 == NO)&&(!_searchBar)) {
            UILabel *label = [UILabel new];
            label.text = NSLocalizedString(@"There is no dialogs", "");
            label.textAlignment = NSTextAlignmentCenter;
            UIView *view = [UIView new];
            [view addSubview:label];
            tableView.backgroundView = view;
            tableView.backgroundColor = [UIColor backgroundColorForChooseDialog];
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(view).with.offset(70);
                make.left.equalTo(view);
                make.right.equalTo(view);
                make.height.equalTo(@60);
            }];
            return 1;
        }
        if (_searchBar) {
            UILabel *label = [UILabel new];
            label.text = NSLocalizedString(@"There is no result", "");
            label.textAlignment = NSTextAlignmentCenter;
            UIView *view = [UIView new];
            [view addSubview:label];
            tableView.backgroundView = view;
            tableView.backgroundColor = [UIColor backgroundColorForChooseDialog];
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(view).with.offset(70);
                make.left.equalTo(view);
                make.right.equalTo(view);
                make.height.equalTo(@60);
            }];
            return 1;
        }
        UIImage *image = [UIImage imageNamed:@"refresh"];
        image = [UIImage imageWithImage:image forSize:CGSizeMake(70, 70)];
        
        UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
        UIView *view = [UIView new];
        [view addSubview:imageView];
        tableView.backgroundView = view;
        tableView.backgroundColor = [UIColor backgroundColorForChooseDialog];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(@100);
            make.center.equalTo(view);
        }];
        [self runSpinAnimationOnView:imageView duration:8 rotations:5 repeat:5];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return 1;
}

@end
