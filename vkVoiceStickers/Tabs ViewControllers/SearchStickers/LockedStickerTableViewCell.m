//
//  LockedStickerTableViewCell.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 16.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "LockedStickerTableViewCell.h"
#import "UIImage+StickersSize.h"
#import <Masonry.h>
#import "UIColor+StickersColor.h"

@implementation LockedStickerTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _viewForZamok = [UIView new];
        UIView *darkBlur = [UIView new];
        darkBlur.backgroundColor = [UIColor colorForSendButton];
        darkBlur.alpha = 0.5;
        [_viewForZamok addSubview:darkBlur];
        
        UIImage *zamok = [UIImage imageNamed:@"zamok"];
        UIImageView *zamokView = [[UIImageView alloc]initWithImage:[UIImage imageWithImage:zamok forSize:CGSizeMake(40, 40)]];
        [_viewForZamok addSubview:zamokView];
        [self.contentView addSubview:_viewForZamok];
        
        [_viewForZamok mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.contentView);
        }];
        [zamokView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_viewForZamok);
        }];
        [darkBlur mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(self.contentView);
            make.center.equalTo(_viewForZamok);
        }];
    }
    return self;
}

@end
