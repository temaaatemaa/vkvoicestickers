//
//  LockedStickerTableViewCell.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 16.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "StickerTableViewCell.h"

@interface LockedStickerTableViewCell : StickerTableViewCell

@property (assign, nonatomic) BOOL isLocked;
@property (strong,nonatomic) UIView *viewForZamok;

@end
