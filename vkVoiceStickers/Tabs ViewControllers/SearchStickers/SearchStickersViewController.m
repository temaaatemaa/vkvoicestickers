//
//  SearchStickersViewController.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 16.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "SearchStickersViewController.h"
#import "UIColor+StickersColor.h"
#import <Masonry.h>
#import "DataAboutStickersFiles.h"
#import "PlayVoiceSticker.h"
#import "StickerTableViewCell.h"
#import "LockedStickerTableViewCell.h"
#import <VKSdk.h>
#import "VkAuth.h"
#import "ChooseDialogViewController.h"
#import "YandexReport.h"
#import "PurchaseMaster.h"


@interface SearchStickersViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *arrayOfAllStickers;
@property (nonatomic, strong) NSArray *filtredArray;
@property (nonatomic, strong) NSDictionary *dataAboutStickers;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic,strong )PlayVoiceSticker *player;
@property (nonatomic, strong)UIView *viewForButtons;

//chosen sticker
@property (nonatomic, assign)NSInteger numberOfAlbum;
@property (nonatomic, strong)NSString *nameOfAlbum;
@property (nonatomic, assign)NSInteger numberOfSound;
@property (nonatomic, strong)NSString *choosenSticker;

@property (nonatomic, strong)UIButton *sendToVkButton;
@property (nonatomic, strong)UIButton *sendToTelegram;
@property (nonatomic, strong)UIButton *sendToWhatsApp;

@property (nonatomic, assign)NSInteger prevSelected;
@end

@implementation SearchStickersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.prevSelected = -1;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.view.backgroundColor = [UIColor colorSoundButton];
    self.navigationController.navigationBar.backgroundColor = [UIColor colorSoundButton];
    self.navigationController.navigationBar.shadowImage = nil;
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = YES;
    [self initArray];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectNull style:UITableViewStylePlain];
    [_tableView registerClass:[StickerTableViewCell class] forCellReuseIdentifier:@"cell2"];
    [_tableView registerClass:[LockedStickerTableViewCell class] forCellReuseIdentifier:@"cell3"];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorForSearchBackground];
    [self.view addSubview:_tableView];
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(85, 0, self.view.frame.size.width-90, 44)];
    _searchBar.delegate = self;
    _searchBar.tag = 333;
    [self.navigationController.navigationBar addSubview:_searchBar];
    
    CGRect rectForStatusBar = [UIApplication sharedApplication].statusBarFrame;
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(rectForStatusBar.size.height);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
}
- (void)initArray
{
    self.dataAboutStickers = [DataAboutStickersFiles getDataAboutStickersFiles];
    NSMutableArray *mutableArrayOfAllStickers = [NSMutableArray new];
    for (NSString *pack in self.dataAboutStickers)
    {
        if ([self.arrayOfPersons containsObject:pack])
        {
            if ([self.dataAboutStickers[pack][@"test"] isEqualToString:@"YES"])
            {
                for (NSDictionary *stickerDict in self.dataAboutStickers[pack][@"stickers"])
                {
                    [mutableArrayOfAllStickers addObject:stickerDict[@"stickerName"]];
                }
            }
            else
            {
                for (NSString *stickerName in self.dataAboutStickers[pack][@"stickers"])
                {
                    [mutableArrayOfAllStickers addObject:stickerName];
                }
            }
        }
    }
    self.arrayOfAllStickers = [mutableArrayOfAllStickers copy];
    self.filtredArray = self.arrayOfAllStickers;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(85, 0, self.view.frame.size.width-90, 44)];
    _searchBar.delegate = self;
    [self.navigationController.navigationBar addSubview:_searchBar];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.view layoutIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.searchBar setAlpha:0];
    [self.searchBar removeFromSuperview];

    for (UIView *view in self.navigationController.navigationBar.subviews)
    {
        if ([view isEqual:self.searchBar]) {
            [view removeFromSuperview];
        }
        if (view.tag == 333)
        {
            [view removeFromSuperview];
        }
    }
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)dealloc
{
    [self.searchBar removeFromSuperview];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF Contains[cd] %@",searchText];
    self.filtredArray = [self.arrayOfAllStickers filteredArrayUsingPredicate:predicate];
    if ([self.filtredArray count] == 0)
    {
        //self.filtredArray = self.arrayOfAllStickers;
    }
    if ([searchText isEqualToString:@""])
    {
        self.filtredArray = self.arrayOfAllStickers;
    }
    [self.tableView reloadData];
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    StickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell2"];
    cell.stickerLabel.text = self.filtredArray[indexPath.row];
    cell.albumLabel.text = [NSString stringWithFormat:@"@%@",[self getAlbumForSticker:self.filtredArray[indexPath.row]]];
    //cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor colorWithRed:190/255.f green:233/255.f blue:251/255.f alpha:1];
    cell.selectedBackgroundView = view;
    if (([self isStickerLocked:self.filtredArray[indexPath.row]])&&(![PurchaseMaster stickersIsBuy]))
    {
        LockedStickerTableViewCell *cellLocked = [tableView dequeueReusableCellWithIdentifier:@"cell3"];
        cellLocked.stickerLabel.text = self.filtredArray[indexPath.row];
        cellLocked.albumLabel.text = [NSString stringWithFormat:@"@%@",[self getAlbumForSticker:self.filtredArray[indexPath.row]]];
        cellLocked.selectionStyle = UITableViewCellSelectionStyleNone;
        return cellLocked;
    }
    else
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.filtredArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.view endEditing:YES];
    [self.searchBar endEditing:YES];
    NSString *sticker = self.filtredArray[indexPath.row];
    self.choosenSticker = sticker;
    NSString *nameOfFile = [self getNameOfFileFromSticker:sticker];
    
    _player = [PlayVoiceSticker new];
    [_player playStickerWithName:nameOfFile];
    [self showSendButtons];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}
- (NSString *)getNameOfFileFromSticker:(NSString *)sticker
{
    NSString *nameOfFile;
    for (NSString *pack in self.dataAboutStickers)
    {
        if ([self.dataAboutStickers[pack][@"test"] isEqualToString:@"YES"])
        {

        }
        else
        {
            for (NSString *stickerName in self.dataAboutStickers[pack][@"stickers"])
            {
                if ([stickerName isEqualToString:sticker])
                {
                    NSLog(@"%lu %lu",(unsigned long)[self.arrayOfPersons indexOfObject:pack],[self.dataAboutStickers[pack][@"stickers"] indexOfObject:stickerName]);
                    self.numberOfAlbum = ((NSNumber *)self.dataAboutStickers[pack][@"soundNumber"]).integerValue;
                    self.numberOfSound = [self.dataAboutStickers[pack][@"stickers"] indexOfObject:stickerName];
                    self.nameOfAlbum = pack;
                    nameOfFile = [NSString stringWithFormat:@"%@_sound_%lu",self.dataAboutStickers[pack][@"soundNumber"],[self.dataAboutStickers[pack][@"stickers"] indexOfObject:sticker]];
                }
            }
        }
    }
    return nameOfFile;
}
- (BOOL)isStickerLocked:(NSString *)sticker
{
    for (NSString *pack in self.dataAboutStickers)
    {
        if ([self.dataAboutStickers[pack][@"test"] isEqualToString:@"YES"])
        {

        }
        else
        {
            for (NSString *stickerName in self.dataAboutStickers[pack][@"stickers"])
            {
                if ([stickerName isEqualToString:sticker])
                {
                    if ([self.dataAboutStickers[pack][@"paid"]isEqualToString:@"YES"])
                    {
                        return YES;
                    }
                }
            }
        }
    }
    return NO;
}
- (NSString *)getAlbumForSticker:(NSString *)sticker
{
    for (NSString *pack in self.dataAboutStickers)
    {
        if ([self.dataAboutStickers[pack][@"test"] isEqualToString:@"YES"])
        {
            if ([self.arrayOfPersons[0]isEqualToString:@"VoiceStickersPack1"])
            {
                for (NSDictionary *dict in self.dataAboutStickers[pack][@"stickers"])
                {
                    if ([dict[@"stickerName"]isEqualToString:sticker])
                    {
                        return pack;
                    }
                }
            }
        }
        else
        {
            for (NSString *stickerName in self.dataAboutStickers[pack][@"stickers"])
            {
                if ([stickerName isEqualToString:sticker])
                {
                    return pack;
                }
            }
        }
    }
    return @"";
}

- (void)showSendButtons
{
    if (_viewForButtons)
    {
        return;
    }
    _viewForButtons = [UIView new];
    [self.view addSubview:_viewForButtons];
    _sendToVkButton = [UIButton new];
    _sendToTelegram = [UIButton new];
    _sendToWhatsApp = [UIButton new];
    
    [_sendToVkButton setBackgroundImage:[UIImage imageNamed:@"vk"] forState:UIControlStateNormal];
    [_sendToTelegram setBackgroundImage:[UIImage imageNamed:@"telegram"] forState:UIControlStateNormal];
    [_sendToWhatsApp setBackgroundImage:[UIImage imageNamed:@"whatsapp"] forState:UIControlStateNormal];
    
    [_sendToVkButton addTarget:self action:@selector(sendToVk) forControlEvents:UIControlEventTouchDown];
    [_sendToTelegram addTarget:self action:@selector(sendToNotVk) forControlEvents:UIControlEventTouchDown];
    [_sendToWhatsApp addTarget:self action:@selector(sendToNotVk) forControlEvents:UIControlEventTouchDown];

    [_viewForButtons addSubview:_sendToVkButton];
    [_viewForButtons addSubview:_sendToTelegram];
    [_viewForButtons addSubview:_sendToWhatsApp];
    
    [_sendToVkButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_viewForButtons);
        make.width.equalTo(_viewForButtons);
        make.height.equalTo(_sendToVkButton.mas_width);
        make.centerX.equalTo(_viewForButtons);
    }];
    [_sendToTelegram mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_sendToVkButton.mas_bottom).with.offset(15);
        make.size.equalTo(_sendToVkButton);
        make.centerX.equalTo(_sendToVkButton);
    }];
    [_sendToWhatsApp mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_sendToTelegram.mas_bottom).with.offset(15);
        make.size.equalTo(_sendToVkButton);
        make.centerX.equalTo(_sendToVkButton);
        make.bottom.equalTo(_viewForButtons);
    }];
    CGFloat width = self.view.frame.size.width*0.17;
    [_viewForButtons mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).with.offset(75);
        make.centerY.equalTo(self.view).with.offset(-40);
        make.width.equalTo(@(width));
    }];
    [self.view layoutIfNeeded];
    [self.view setNeedsLayout];
    [UIView animateWithDuration:0.5 animations:^{
        [_viewForButtons mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view).with.offset(-5);
            make.centerY.equalTo(self.view).with.offset(-40);
            make.width.equalTo(@(width));
        }];
        [self.view layoutIfNeeded];
        [self.view setNeedsLayout];
    }];

}
- (void)sendToVk
{
    if ([self isStickerLocked:self.choosenSticker])
    {
        if (![PurchaseMaster stickersIsBuy])
        {
            [self showAlertToBuyVIP];
            return;
        }
    }
    
    if (![VKSdk accessToken].accessToken)
    {
        [self showAlertWhenUserIsNotLogin];
    }
    
    
    ChooseDialogViewController *choose = [ChooseDialogViewController new];

    
    choose.nameOfSound = [NSString stringWithFormat:@"%ld_sound_%ld",(long)self.numberOfAlbum,(long)self.numberOfSound];
    [self presentViewController:choose animated:YES completion:nil];
    
    
    [YandexReport reportEventWithName:@"Send sticker"
                           parameters:@{
                                        @"albumName":self.nameOfAlbum,
                                        @"sticker":self.choosenSticker
                                        }];
    [YandexReport reportEventWithName:@"Send sticker via VK"
                           parameters:@{
                                        @"albumName":self.nameOfAlbum,
                                        @"sticker":self.choosenSticker
                                        }];
}
- (void)sendToNotVk
{
    if ([self isStickerLocked:self.choosenSticker])
    {
        if (![PurchaseMaster stickersIsBuy])
        {
            [self showAlertToBuyVIP];
            return;
        }
    }
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSURL *fileURL;
        
            fileURL = [[NSBundle mainBundle]
                       URLForResource:[NSString stringWithFormat:@"%ld_sound_%ld",(long)self.numberOfAlbum,(long)self.numberOfSound] withExtension:@"mp3"];
        
        if (!fileURL)
        {
            return ;
        }
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:@[fileURL] applicationActivities:nil];
        activityViewController.excludedActivityTypes = @[UIActivityTypeMail,UIActivityTypeAirDrop, UIActivityTypeMessage,UIActivityTypeCopyToPasteboard];
        activityViewController.popoverPresentationController.sourceView = self.view;
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self presentViewController:activityViewController animated:YES completion:^{
                NSLog(@"open");
            }];
        });
    });
    
    [YandexReport reportEventWithName:@"Send sticker"
                           parameters:@{
                                        @"albumName":self.nameOfAlbum,
                                        @"sticker":self.choosenSticker
                                        }];
    
    [YandexReport reportEventWithName:@"Send sticker not vk"
                           parameters:@{
                                        @"albumName":self.nameOfAlbum,
                                        @"sticker":self.choosenSticker
                                        }];
}
- (void)showAlertWhenUserIsNotLogin
{
    NSString *title = NSLocalizedString(@"Account VK","");
    NSString *message = NSLocalizedString(@"You need log in to VK to send voice stickers from you. Access to documents is needed for save voice messages. Access to friends need to choose dialog and send message.", "");
    NSString *ok = NSLocalizedString(@"OK","");
    NSString *cancel = NSLocalizedString(@"Cancel","");
    
    UIAlertController *al = [UIAlertController alertControllerWithTitle:title
                                                                message:message
                                                         preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        VkAuth *auth = [[VkAuth alloc]init];
        [auth autentification:self];
        [YandexReport reportEventAuth];
        if ([VKSdk isLoggedIn])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self sendToVk];
            });
        }
    }];
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [YandexReport reportEventCancelAuth];
    }];
    [al addAction:cancelButton];
    [al addAction:okButton];
    
    [self presentViewController:al animated:YES completion:^{
        if ([VKSdk isLoggedIn])//????????????
        {
            [self sendToVk];
        }
    }];
}
- (void)showAlertToBuyVIP
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Стикер не доступен!" message:@"Для доступу к данному стикеру требуется VIP статус. Купить VIP статус можно в настройках." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okButton];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}
@end
