//
//  StickerTableViewCell.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 16.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StickerTableViewCell : UITableViewCell

@property (nonatomic, strong)UILabel *stickerLabel;
@property (nonatomic, strong)UILabel *albumLabel;

@end
