//
//  StickerTableViewCell.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 16.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "StickerTableViewCell.h"
#import <Masonry.h>

@implementation StickerTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _stickerLabel = [UILabel new];
        _albumLabel = [UILabel new];
        _stickerLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
        _albumLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightThin];
        
        self.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        [self.contentView addSubview:_stickerLabel];
        [self.contentView addSubview:_albumLabel];
        
        [_stickerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).with.offset(5);
            make.left.equalTo(self.contentView).with.offset(10);
        }];
        [_albumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_stickerLabel.mas_bottom).with.offset(5);
            make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        }];
    }
    return self;
}


@end
