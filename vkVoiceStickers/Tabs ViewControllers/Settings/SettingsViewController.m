//
//  SettingsViewController.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "SettingsViewController.h"
#import <Masonry.h>
#import <VKApi.h>
#import "VkAuth.h"
#import "YandexReport.h"
#import "UIColor+StickersColor.h"
#import "PurchaseMaster.h"
@interface SettingsViewController ()<UITableViewDataSource,UITableViewDelegate,PurchaseMasterDelegate>

@property (strong, nonatomic)UITableView *tableView;
@property (strong,nonatomic)NSString *firstName;
@property (strong,nonatomic)NSString *lastName;
@property (strong, nonatomic)NSTimer *waitingTimer;

@property (strong, nonatomic)PurchaseMaster *purchaseMaster;
@property (strong,nonatomic)UIView *viewForLoadingForStore;


@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getUserInfo];
    [YandexReport reportEventWithName:@"Settings" parameters:@{}];
    
    
    UIView *header = [[UIView alloc]initWithFrame:CGRectNull];
    header.backgroundColor = [UIColor colorForHeaderInSettings];
    
    [self.view addSubview:header];
    
    
    UILabel *label = [[UILabel alloc]init];
    label.text = NSLocalizedString(@"Settings", "");
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:23 weight:UIFontWeightMedium];
    
    [header addSubview:label];
    
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectNull style:UITableViewStyleGrouped];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor backgroundColorForSettings];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_tableView];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(header).with.offset(5);
        make.left.equalTo(header);
        make.right.equalTo(header);
    }];
    [header mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.height.equalTo(@(self.view.frame.size.height*0.13));
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(header.mas_bottom);
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getUserInfo];
    [self.tableView reloadData];
}

#pragma mark - Custom Accessors

- (PurchaseMaster *)purchaseMaster
{
    if (!_purchaseMaster) {
        _purchaseMaster = [PurchaseMaster new];
        _purchaseMaster.delegate = self;
    }
    return _purchaseMaster;
}

#pragma mark - Private Methods

- (void)showAlertWithRateLinkWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *cancleButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:cancleButton];
    
    UIAlertAction *linkButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Rate", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/appName/id1357370653?mt=8&action=write-review"];
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }];
    [alertController addAction:linkButton];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)showAlertWithTelegramLinkWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *linkButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Go to Telegram", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSURL *url = [NSURL URLWithString:@"https://t.me/vkVoiceStickersIos"];
        [YandexReport reportEventWithName:@"go to telegram from settings" parameters:@{}];
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }];
    [alertController addAction:linkButton];
    UIAlertAction *linkButtonToVK = [UIAlertAction actionWithTitle:NSLocalizedString(@"Группа ВК", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSURL *url = [NSURL URLWithString:@"https://vk.com/voicestickersios"];
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        [YandexReport reportEventWithName:@"go to vk group from settings" parameters:@{}];
    }];
    [alertController addAction:linkButtonToVK];
    UIAlertAction *cancleButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:cancleButton];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)showAlert:(NSString *)title message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okButton];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)showLoadingViewForStore
{
    if (_viewForLoadingForStore)
    {
        return;
    }
    _viewForLoadingForStore = [UIView new];
    [self.view addSubview:_viewForLoadingForStore];
    _viewForLoadingForStore.backgroundColor = [UIColor blackColor];
    _viewForLoadingForStore.alpha = 0.001;
    [UIView animateWithDuration:0.5 animations:^{
        _viewForLoadingForStore.alpha = 0.5;
    }];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_viewForLoadingForStore addSubview:indicator];
    [indicator startAnimating];
    [_viewForLoadingForStore mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(self.view);
        make.center.equalTo(self.view);
    }];
    [indicator mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(_viewForLoadingForStore);
    }];
}

-(void)getUserInfo
{
    VkAuth *auth = [[VkAuth alloc]init];
    [auth autentification:self];
    
    VKRequest* audioReq = [VKApi requestWithMethod:@"account.getProfileInfo" andParameters:@{}];
    [audioReq executeWithResultBlock:
     ^(VKResponse * response){
         
         NSDictionary *dict = [VkAuth ResponseToDict:response];
         _firstName = [dict objectForKey:@"first_name"];
         _lastName = [dict objectForKey:@"last_name"];
         [self.tableView reloadData];
     } errorBlock:^(NSError * error) {
         if (error.code != VK_API_ERROR) {
             [error.vkError.request repeat];
         }
         else {
             NSLog(@"VK error: %@", error);
         }
     }];
}


#pragma mark - TableViewDelegate

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    switch (indexPath.row) {
        case 0:
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (_firstName) {
                cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",_firstName,_lastName];
                cell.textLabel.textAlignment = NSTextAlignmentCenter;
                cell.textLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightHeavy];
            }
            break;
        case 1:
            if ([VKSdk isLoggedIn])
            {
                cell.textLabel.text = NSLocalizedString(@"Log off VK", "");
            }
            else
            {
                cell.textLabel.text = NSLocalizedString(@"Log in VK", "");
            }
            break;
        case 2:
            cell.textLabel.text = NSLocalizedString(@"Rate on App Store", "");
            break;
        case 3:
            cell.textLabel.text = NSLocalizedString(@"About", "");
            break;
        case 4:
            cell.textLabel.text = NSLocalizedString(@"Feedback", "");
            break;
        case 5:
            if (![PurchaseMaster stickersIsBuy])
            {
                cell.textLabel.text = NSLocalizedString(@"Buy VIP status", "");
            }
            else
            {
                cell.textLabel.text = NSLocalizedString(@"You have VIP status😎", "");
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            break;
        case 6:
            cell.textLabel.text = NSLocalizedString(@"Restore VIP status", "");
            break;
        default:
            break;
    }
    cell.backgroundColor = [UIColor backgroundColorForSettingsTableViewCell];
    cell.textLabel.textColor = [UIColor fontColor];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([PurchaseMaster stickersIsBuy])
    {
        return 6;
    }
    return 7;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = self.view.frame.size.height*0.095;
    switch (indexPath.row) {
        case 0:
            return 0.13 * self.view.frame.size.height;
            break;
        case 1:
            return height;
            break;
        case 2:
            return height;
            break;
        case 3:
            return height;
            break;
        case 4:
            return height;
            break;
        case 5:
            return height;
            break;
        case 6:
            return height;
            break;
        default:
            break;
    }
    return 50;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [UIView new];
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [UIView new];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
   

    switch (indexPath.row) {
        case 0:
            return;
            break;
        case 1:
            if ([VKSdk isLoggedIn])
            {
                [VKSdk forceLogout];
                [self showAlert:NSLocalizedString(@"Log off from VK", "")
                        message:NSLocalizedString(@"You have been log off", "")];
                _firstName = NSLocalizedString(@"You didn't log on", "");
                _lastName = @"";
                [self.tableView reloadData];
                [YandexReport reportEventWithName:@"Exit vk" parameters:@{}];
            }
            else
            {
                VkAuth *auth = [[VkAuth alloc]init];
                [auth autentification:self];
                [self getUserInfo];
                [self.tableView reloadData];
                _waitingTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                    NSLog(@"timer");
                    if ([VKSdk isLoggedIn]) {
                         [self getUserInfo];
                        [timer invalidate];
                    }
                }];
                [_waitingTimer fire];
                [YandexReport reportEventAuth];
            }
            break;
        case 2:
            [self showAlertWithRateLinkWithTitle:NSLocalizedString(@"Rate", "") withMessage:NSLocalizedString(@"If you like app please rate it on App Store", "")];
            [YandexReport reportEventWithName:@"Give mark" parameters:@{}];
            
            break;
        case 3:
            [self showAlertWithTelegramLinkWithTitle:NSLocalizedString(@"About", "") withMessage:NSLocalizedString(@"If you have ideas about app or you have problems please write to @vkVoiceStickersIos. Fast answer. Version: 1.2", "")];
            
            [YandexReport reportEventWithName:@"About" parameters:@{}];
            break;
        case 4:
            [self showAlertWithTelegramLinkWithTitle:NSLocalizedString(@"Feedback", "") withMessage:NSLocalizedString(@"If you have ideas about app or you have problems please write to @vkVoiceStickersIos. Fast answer.", "")];
            
            [YandexReport reportEventWithName:@"Feedback" parameters:@{}];
            break;
        case 5:
            if ([PurchaseMaster stickersIsBuy])
            {
                return;
            }
            [self.purchaseMaster buyStickers];
            [self showLoadingViewForStore];
            [YandexReport reportEventWithName:@"want to buy" parameters:@{}];
            //[PurchaseMaster buyForTest];
            break;
        case 6:
            [self.purchaseMaster restore];
            [self showLoadingViewForStore];
            [YandexReport reportEventWithName:@"want to restore" parameters:@{}];
            //[PurchaseMaster deleteBuySettings];
            break;
        default:
            break;
    }

    return;
}


#pragma mark - PurchaseMasterDelegate

- (void)vipDidReceive:(PurchaseMaster *)purchaseMaster
{
    [self.tableView reloadData];
    [YandexReport reportEventWithName:@"Has bought" parameters:@{}];
}
- (void)didExitFromStore:(PurchaseMaster *)purchaseMaster
{
    [_viewForLoadingForStore removeFromSuperview];
    _viewForLoadingForStore = nil;
}

@end
