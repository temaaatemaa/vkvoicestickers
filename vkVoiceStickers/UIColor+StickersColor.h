//
//  UIColor+CEColor.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 19.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor(StickersColor)

+ (UIColor *)mainColor;

+ (UIColor *)additionalColor;

+ (UIColor *)fontColor;

+ (UIColor *)colorForCollectionViewCell;

+ (UIColor *)colorSoundButton;

+ (UIColor *)backgroundColorOfGallary;
+ (UIColor *)backgroundColorOfCollectionView;

+ (UIColor *)colorForSendButton;

+ (UIColor *)backgroundColorForChooseDialog;

+ (UIColor *)backgroundColorForSettingsTableViewCell;
+ (UIColor *)backgroundColorForSettings;

+ (UIColor *)colorForCancelButtonInDialogs;
+ (UIColor *)colorForHeaderInSettings;
+ (UIColor *)colorForHeaderInDialogs;

+ (UIColor *)colorForSearchStickersButton;
+ (UIColor *)colorForSearchBackground;
//+ (UIColor *)ceRefreshColor;
//
//+ (UIColor *)ceGreenColorForPersentageInMarketAndWalletCell;
//
//+ (UIColor *)ceGreenColorForPriceInMarketAndWalletCell;
//
//+ (UIColor *)ceRedColorInMarketAndWalletCell;
//
//+ (UIColor *)ceColorForTabBarItems;
//
//+ (UIColor *)ceFontColorForMarketAndWalletCell;
//
//+ (UIColor *)ceBackgroundColor;
//
//+ (UIColor *)ceColorForTabBarController;
//
//+ (UIColor *)ceColorForTabBarTint;
//
//+ (UIColor *)ceColorForBackgroundViewOfPopoverViewInCEGraph;

@end
