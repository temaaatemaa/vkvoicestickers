//
//  AppDelegate.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "AppDelegate.h"
#import <VKSdk.h>
#import "GallaryViewController.h"
#import "SettingsViewController.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>
#import <YandexMobileMetricaPush/YandexMobileMetricaPush.h>
#import <UserNotifications/UserNotifications.h>
#import "YandexReport.h"
#import "DreamsViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


+ (void)initialize
{
    if ([self class] == [AppDelegate class]) {

        YMMYandexMetricaConfiguration *configuration = [[YMMYandexMetricaConfiguration alloc] initWithApiKey:@"f846545d-b71e-4820-98e8-5ec70ff08bcc"];
        [YMMYandexMetrica activateWithConfiguration:configuration];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self initWindow];
    
    
    if ([UNUserNotificationCenter class] != Nil) {
        [UNUserNotificationCenter currentNotificationCenter].delegate =
        [YMPYandexMetricaPush userNotificationCenterDelegate];
    }
    
     [YMPYandexMetricaPush handleApplicationDidFinishLaunchingWithOptions:launchOptions];
    
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        if (NSClassFromString(@"UNUserNotificationCenter") != Nil) {
            // iOS 10.0 and above
            UNAuthorizationOptions options =
            UNAuthorizationOptionAlert |
            UNAuthorizationOptionBadge |
            UNAuthorizationOptionSound;
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            [center requestAuthorizationWithOptions:options completionHandler:^(BOOL granted, NSError *error) {
                NSLog(@"requestAuthorizationWithOptions:");
                if (error) {
                    NSLog(@"%@",error);
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [application registerForRemoteNotifications];
                    });
                }
            }];
        }
    }
    
    return YES;
}
-(void)initWindow{
    self.window = [UIWindow new];
    
    GallaryViewController *vc = [GallaryViewController new];
    UINavigationController *vcNC = [[UINavigationController alloc]initWithRootViewController:vc];
    vcNC.tabBarItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"Stickers",nil) image:[UIImage imageNamed:@"stickers"] tag:0];
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DreamsViewController *dreamsVC = [story instantiateViewControllerWithIdentifier:@"Dreams"];
    dreamsVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"Wishes", @"") image:[UIImage imageNamed:@"star9090"] tag:2];
    
    SettingsViewController *settignsVC = [SettingsViewController new];
    settignsVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"Settings", @"") image:[UIImage imageNamed:@"settings1"] tag:1];
    
    NSArray *arraysOfVC = [NSArray arrayWithObjects:vcNC,dreamsVC,settignsVC, nil];
    
    UITabBarController *tabBarController = [UITabBarController new];
    tabBarController.viewControllers = arraysOfVC;
    tabBarController.tabBar.barStyle = UIBarStyleBlack;
    
    self.window.rootViewController = tabBarController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Если до этого шага не была инициализирована библиотека AppMetrica SDK, вызов метода приведет к аварийной остановке приложения.
    [YandexReport reportEventWithName:@"didRegisterForRemoteNotidications" parameters:@{}];
#ifdef DEBUG
    YMPYandexMetricaPushEnvironment pushEnvironment = YMPYandexMetricaPushEnvironmentDevelopment;
#else
    YMPYandexMetricaPushEnvironment pushEnvironment = YMPYandexMetricaPushEnvironmentProduction;
#endif
    [YMPYandexMetricaPush setDeviceTokenFromData:deviceToken pushEnvironment:pushEnvironment];
    NSLog(@"%@ %lu",deviceToken,(unsigned long)pushEnvironment);
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"didFAILtoRegisterRemoteNotification %@",error.localizedDescription);
    [YandexReport reportEventWithName:@"didFAILtoRegisterRemoteNotification" parameters:@{@"error":error.localizedDescription}];
}
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [YMPYandexMetricaPush handleRemoteNotification:userInfo];
    NSLog(@"didReceiveRemoteNotification");
}
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [YMPYandexMetricaPush handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
    NSLog(@"didReceiveRemoteNotification:fetchCompletionHandler:");
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    [VKSdk processOpenURL:url fromApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"vkVoiceStickers"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
