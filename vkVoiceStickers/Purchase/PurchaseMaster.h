//
//  PurchaseMaster.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 09.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SettingsViewController.h"
@class PurchaseMaster;


@protocol PurchaseMasterDelegate

- (void)vipDidReceive:(PurchaseMaster *)purchaseMaster;
- (void)didExitFromStore:(PurchaseMaster *)purchaseMaster;

@end


@interface PurchaseMaster : NSObject

- (void)restore;
- (void)buyStickers;
+ (BOOL)stickersIsBuy;
+ (void)buyForTest;
+ (void)deleteBuySettings;

@property (weak, nonatomic)id<PurchaseMasterDelegate> delegate;

@end
