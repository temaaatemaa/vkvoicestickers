//
//  PurchaseMaster.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 09.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "PurchaseMaster.h"
#import <StoreKit/StoreKit.h>
#import "YandexReport.h"

#define kRemoveAdsProductIdentifier @"com.voicestickers.vip"


@interface PurchaseMaster() <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (strong,nonatomic)SKProductsRequest *productsRequest;

@end


@implementation PurchaseMaster

- (void)buyStickers
{
    NSLog(@"Пользователь запросил удаление рекламы");
    
    if([SKPaymentQueue canMakePayments])
    {
        NSLog(@"Пользователь может совершить покупку");
        
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"product_ids"
                                             withExtension:@"plist"];
        NSArray *productIdentifiers = [NSArray arrayWithContentsOfURL:url];
        NSLog(@"ids %@",productIdentifiers);
        _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
        _productsRequest.delegate = self;
        [_productsRequest start];
        
    }
    else
    {
        NSLog(@"Пользователь не может производить платежи из-за родительского контроля");
    }
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    SKProduct *validProduct = nil;
    NSUInteger count = [response.products count];
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Продукты доступны!");
        [self purchase:validProduct];
        [YandexReport reportEventWithName:@"products avalible" parameters:@{}];
    }
    else if(!validProduct){
        NSLog(@"Нет доступных продуктов");
        [self.delegate didExitFromStore: self];
    }
}

- (void)purchase:(SKProduct *)product
{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)restore
{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"Получены восстановленные транзакции: %lu", (unsigned long)queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions)
    {
        if(transaction.transactionState == SKPaymentTransactionStateRestored)
        {
            NSLog(@"Transaction state -> Restored");
            [self addVIPAccess];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for(SKPaymentTransaction *transaction in transactions)
    {

        switch(transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                [self addVIPAccess];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                NSLog(@"Transaction state -> Purchased");
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Transaction state -> Restored");
                [self.delegate didExitFromStore: self];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                if(transaction.error.code == SKErrorPaymentCancelled)
                {
                    NSLog(@"Transaction state -> Cancelled");
                    [YandexReport reportEventWithName:@"cancel to buy when see price" parameters:@{}];
                    [self.delegate didExitFromStore: self];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateDeferred:
                NSLog(@"Wait Wait Wait!");
                break;
        }
    }

}

- (void)addVIPAccess
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"buyAllStickers"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self.delegate vipDidReceive:self];
    [self.delegate didExitFromStore: self];
    [YandexReport reportEventWithName:@"purchase master buy" parameters:@{}];
}

+ (BOOL)stickersIsBuy
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"buyAllStickers"])
    {
        return YES;
    }
    return NO;
}

+ (void)buyForTest
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"buyAllStickers"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)deleteBuySettings
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"buyAllStickers"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
