//
//  UIImage+StickersSize.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 16.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(StickersSize)

+ (UIImage *)imageWithImage:(UIImage *)image forSize:(CGSize)size;

@end
