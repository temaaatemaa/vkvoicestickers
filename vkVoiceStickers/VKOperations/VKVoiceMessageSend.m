//
//  VKVoiceMessageSend.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "VKVoiceMessageSend.h"
#import <AFNetworking.h>
#import "YandexReport.h"
#import <VKSdk.h>

@interface VKVoiceMessageSend()

@property (nonatomic, copy) NSString *sound;
@property (nonatomic, copy) NSString *userID;
@property (nonatomic, copy) NSNumber *chatID;

@end

@implementation VKVoiceMessageSend

#pragma mark - Public Methods

-(void)sendVoiceMessageWith:(NSString *)sound toUserId:(NSString *)userID;
{
    _sound = sound;
    _userID = userID;
    
    [self getURLForUpload];
}

-(void)sendVoiceMessageWith:(NSString *)sound toChatId:(NSNumber *)chatID
{
    _sound = sound;
    _chatID = chatID;
    
    [self getURLForUpload];
}

#pragma mark - Private Methods

-(void)getURLForUpload
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    NSString *targetUrl;
    if (_chatID) {
        targetUrl = [NSString stringWithFormat:@"https://api.vk.com/method/%@?%@&access_token=%@&v=5.73",
                           @"docs.getMessagesUploadServer",
                           [NSString stringWithFormat:@"type=audio_message&peer_id=%@",@(2000000000+_chatID.intValue)],
                           [VKSdk accessToken].accessToken];
    }
    else
    {
        targetUrl = [NSString stringWithFormat:@"https://api.vk.com/method/%@?%@&access_token=%@&v=5.73",
                               @"docs.getMessagesUploadServer",
                               [NSString stringWithFormat:@"type=audio_message&peer_id=%@",_userID],
                               [VKSdk accessToken].accessToken];
    }
    
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@",dict);
        if (dict[@"response"][@"upload_url"]) {
            [self uploadToUrl:dict[@"response"][@"upload_url"]];
        }
        
    }];
    [task resume];
}

-(void)uploadToUrl:(NSString *)urfForUpload
{

    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:_sound withExtension:@"mp3"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager POST:urfForUpload parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileURL:fileURL name:@"file" error:nil];
        
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"Response: %@", responseObject);
        [self save:responseObject[@"file"]];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}
     
-(void)save:(NSString *)file
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"https://api.vk.com/method/docs.save" parameters:@{@"file":file,@"access_token":[VKSdk accessToken].accessToken, @"v":@"5.73"} progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self send:responseObject];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void)send:(NSDictionary *)respond
{
    NSString *attach = [NSString stringWithFormat:@"doc%@_%@",respond[@"response"][0][@"owner_id"],respond[@"response"][0][@"id"]];
    NSDictionary *parametrs;
    if (_chatID) {
        parametrs = @{
                                    @"peer_id":@(2000000000+_chatID.intValue),
                                    @"attachment":attach,
                                    @"access_token":[VKSdk accessToken].accessToken,
                                    @"v":@"5.73"
                                    };
    }
    else
    {
        parametrs = @{
                                    @"user_id":_userID,
                                    @"attachment":attach,
                                    @"access_token":[VKSdk accessToken].accessToken,
                                    @"v":@"5.73"
                                    };
    }

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"https://api.vk.com/method/messages.send" parameters:parametrs
        progress:nil
        success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self.delegate messageDidSend:self withResponse:responseObject];
        [YandexReport reportEventWithName:@"send message with attachment" parameters:@{}];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self.delegate messageDidntSend:self withError:error];
        [YandexReport reportError];
        [YandexReport reportEventWithName:@"didnt send message with attachment" parameters:@{@"error":error.localizedDescription}];
    }];
}
@end
