//
//  DialogsAndFriendsDownload.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "DialogsAndFriendsDownload.h"
#import <VKSdk.h>
#import <AFNetworking.h>
#import "VkAuth.h"

@interface DialogsAndFriendsDownload()

@property (nonatomic, copy) NSString *accessTokenForVK;

@end


@implementation DialogsAndFriendsDownload

#pragma mark - Custom Accessors

- (NSString *)accessTokenForVK
{
    return [VKSdk accessToken].accessToken;
}

#pragma mark - Public Methods

- (void)downloadDialogsWithOffset:(NSInteger)offset
{
    if (!self.accessTokenForVK)
    {
        return;
    }
    NSDictionary *parametrs = @{
                                @"offset":@(offset),
                                @"count":@"50",
                                @"access_token":self.accessTokenForVK,
                                @"v":@"5.73"
                                };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"https://api.vk.com/method/messages.getDialogs" parameters:parametrs
        progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
             [self.delegate dialogs:responseObject didDownload:self withOffset:offset];
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

-(void)downloadInfoAboutUsers:(NSString *)usersIDs
{
    if (!self.accessTokenForVK)
    {
        return;
    }
    NSDictionary *parametrs = @{
                                @"user_ids":usersIDs,
                                @"fields":@"photo_100,online",
                                @"access_token":self.accessTokenForVK,
                                @"v":@"5.73"
                                };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"https://api.vk.com/method/users.get" parameters:parametrs
        progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
             [self.delegate usersInfo:responseObject didDownload:self];
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

-(void)downloadInfoAboutGroups:(NSString *)groupIDs
{
    if (!self.accessTokenForVK)
    {
        return;
    }
    
    NSDictionary *parametrs = @{
                                @"group_ids":groupIDs,
                                @"fields":@"photo_100",
                                @"access_token":self.accessTokenForVK,
                                @"v":@"5.73"
                                };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"https://api.vk.com/method/groups.getById" parameters:parametrs
        progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
             [self.delegate groupsInfo:responseObject didDownload:self];
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

-(void)downloadFriends
{
    if (!self.accessTokenForVK)
    {
        return;
    }
    
    NSDictionary *parametrs = @{
                                @"order":@"hints",
                                @"fields":@"photo_100",
                                @"access_token":self.accessTokenForVK,
                                @"v":@"5.73"
                                };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"https://api.vk.com/method/friends.get" parameters:parametrs
        progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
             [self.delegate friends:responseObject didDownload:self];
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

@end
