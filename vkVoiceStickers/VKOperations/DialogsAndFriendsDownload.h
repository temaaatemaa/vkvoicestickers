//
//  DialogsAndFriendsDownload.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DialogsAndFriendsDownload;


@protocol DialogsAndFriendsDownloadDelegate
@required

-(void)dialogs:(NSDictionary *)dialogs didDownload:(DialogsAndFriendsDownload *)dialogsAndFriendsDownload withOffset:(NSInteger)offset;

-(void)usersInfo:(NSDictionary *)usersInfo didDownload:(DialogsAndFriendsDownload *)download;

-(void)groupsInfo:(NSDictionary *)groupsInfo didDownload:(DialogsAndFriendsDownload *)download;

-(void)friends:(NSDictionary *)friends didDownload:(DialogsAndFriendsDownload *)download;

@end


@interface DialogsAndFriendsDownload : NSObject

@property (nonatomic, weak) id<DialogsAndFriendsDownloadDelegate> delegate;

-(void)downloadDialogsWithOffset:(NSInteger)offset;

-(void)downloadInfoAboutUsers:(NSString *)usersIDs;

-(void)downloadInfoAboutGroups:(NSString *)groupIDs;

-(void)downloadFriends;

@end
