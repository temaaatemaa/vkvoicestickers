//
//  VKVoiceMessageSend.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
@class VKVoiceMessageSend;

@protocol VKVoiceMessageSendDelegate

-(void)messageDidSend:(VKVoiceMessageSend *)VKVoiceMessageSend withResponse:(NSDictionary *)response;
-(void)messageDidntSend:(VKVoiceMessageSend *)VKVoiceMessageSend withError:(NSError *)error;

@end


@interface VKVoiceMessageSend : NSObject

@property (weak, nonatomic)id<VKVoiceMessageSendDelegate> delegate;

-(void)sendVoiceMessageWith:(NSString *)sound toUserId:(NSString *)userID;
-(void)sendVoiceMessageWith:(NSString *)sound toChatId:(NSNumber *)chatID;

@end
