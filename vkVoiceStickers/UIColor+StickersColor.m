//
//  UIColor+CEColor.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 19.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import "UIColor+StickersColor.h"

@implementation UIColor(StickersColor)

+ (UIColor *)mainColor
{
    return [UIColor grayColor];
}

+ (UIColor *)fontColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)colorForCollectionViewCell;
{
    return [UIColor whiteColor];
}

+ (UIColor *)colorSoundButton
{
    return [self colorForSendButton];
}

+ (UIColor *)backgroundColorOfGallary
{
    return [self mainColor];
}
+ (UIColor *)backgroundColorOfCollectionView
{
    return [self mainColor];
}

+ (UIColor *)colorForSendButton
{
    //return [UIColor colorWithRed:60.f/255.f green:60.f/255.f blue:60.f/255.f alpha:1];
    return [self colorForHeaderInDialogs];
}

+ (UIColor *)backgroundColorForChooseDialog
{
    return [UIColor whiteColor];
}

+ (UIColor *)backgroundColorForSettingsTableViewCell
{
    return [UIColor colorWithRed:90.f/255.f green:90.f/255.f blue:90.f/255.f alpha:1];
}
+ (UIColor *)backgroundColorForSettings
{
    return [UIColor mainColor];
}
+ (UIColor *)colorForHeaderInDialogs
{
    //return [UIColor colorWithRed:60.f/255.f green:60.f/255.f blue:60.f/255.f alpha:1];
    return [UIColor colorWithRed:0.f/255.f green:150.f/255.f blue:215.f/255.f alpha:1];
}
+ (UIColor *)colorForCancelButtonInDialogs
{
    return [self colorForHeaderInDialogs];
}
+ (UIColor *)colorForHeaderInSettings
{
    return [UIColor colorWithRed:60.f/255.f green:60.f/255.f blue:60.f/255.f alpha:1];
}
+ (UIColor *)colorForSearchStickersButton
{
    return [self colorForHeaderInDialogs];
}
+ (UIColor *)colorForSearchBackground
{
    return [UIColor colorWithRed:104.f/255.f green:203.f/255.f blue:237.f/255.f alpha:1];
}
@end
