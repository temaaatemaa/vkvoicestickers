//
//  UIImage+StickersSize.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 16.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "UIImage+StickersSize.h"

@implementation UIImage(StickersSize)

+ (UIImage *)imageWithImage:(UIImage *)image forSize:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
