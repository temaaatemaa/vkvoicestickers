//
//  DownloadArrayOfPersons.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 07.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "DownloadArrayOfPersons.h"

static NSString *const CEErrorForTimeOut = @"The request timed out.";
static NSString *const CEErrorForOfflineInternet = @"Вероятно, соединение с Интернетом прервано.";

static NSString *const CEURLForArrayOfPersons = @"https://demo3029246.mockable.io/12";


@interface DownloadArrayOfPersons()<NSURLSessionDataDelegate>

@property (nonatomic, strong) NSURLSession *session;

@end


@implementation DownloadArrayOfPersons

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
        _session = [NSURLSession sessionWithConfiguration:configuration
                                                 delegate:self
                                            delegateQueue:nil];
    }
    return self;
}
- (void)downloadJSONAboutStickers
{
    static int counter = 0;
    NSString *URLString = CEURLForArrayOfPersons;

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData * _Nullable data,
                                                                     NSURLResponse * _Nullable response,
                                                                     NSError * _Nullable error) {
                                                     counter++;
                                                     if (![self handleErrorFromDataTask:error withData:data])
                                                     {
                                                         if (counter<10)
                                                         {
                                                             [self downloadJSONAboutStickers];
                                                         }
                                                         return;
                                                     }
                                                     NSDictionary *stickersData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                     if (!stickersData[@"arrayOfPersons"])
                                                     {
                                                         if (counter<10)
                                                         {
                                                             [self downloadJSONAboutStickers];
                                                         }
                                                         return;
                                                     }
                                                     [self.delegate arrayOfPersonsDidDownload:self dict:stickersData];
                                                 }];
    [task resume];
}

- (int)handleErrorFromDataTask:(NSError *)error withData:(NSData *)data
{
    if (error)
    {
        NSLog(@"%@, %@",error,[error userInfo]);
        NSString *description = error.userInfo[@"NSLocalizedDescription"];
        if ([description isEqualToString:CEErrorForTimeOut])
        {
            NSLog(@"TIMEOUT");
            return 0;
        }
        if ([description isEqualToString:CEErrorForOfflineInternet])
        {
            NSLog(@"NO INTERNET");
            return 0;
        }
    }
    if (!data)
    {
        return 0;
    }
    return 1;
}
@end
