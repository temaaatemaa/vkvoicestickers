//
//  DataAboutStickersFiles.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 10.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadArrayOfPersons.h"
@class DataAboutStickersFiles;


@protocol DataAboutStickersDelegate

- (void)arrayOfPersonsHasBeenReceived:(DataAboutStickersFiles *)dataAboutStickers withData:(NSDictionary *)dict;

@end


@interface DataAboutStickersFiles : NSObject <DownloadArrayOfPersonsDelegate>


+ (NSDictionary *)getDataAboutStickersFiles;
- (void)getArrayOfPersonsFromInternet;

@property (nonatomic, weak)id<DataAboutStickersDelegate> delegate;


@end
