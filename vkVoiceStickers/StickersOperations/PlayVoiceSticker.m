//
//  PlayVoiceSticker.m
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "PlayVoiceSticker.h"
#import <AVFoundation/AVFoundation.h>

@interface PlayVoiceSticker()
@property (nonatomic,strong) AVAudioPlayer *player;

@end
@implementation PlayVoiceSticker

-(void)playStickerWithName:(NSString *)name
{
    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:name withExtension:@"mp3"];
    [[AVAudioSession sharedInstance]//play with mute
     setCategory: AVAudioSessionCategoryPlayback
     error: nil];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    [_player play];
}

@end
