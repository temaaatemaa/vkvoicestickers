//
//  PlayVoiceSticker.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 08.03.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlayVoiceSticker : NSObject

- (void)playStickerWithName:(NSString *)name;

@end
