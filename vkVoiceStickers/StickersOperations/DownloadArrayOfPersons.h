//
//  DownloadArrayOfPersons.h
//  vkVoiceStickers
//
//  Created by Artem Zabludovsky on 07.04.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DownloadArrayOfPersons;


@protocol DownloadArrayOfPersonsDelegate

- (void)arrayOfPersonsDidDownload:(DownloadArrayOfPersons *)DownloadArrayOfPersons dict:(NSDictionary *)dictWithData;

@end


@interface DownloadArrayOfPersons : NSObject


@property (nonatomic, weak) id<DownloadArrayOfPersonsDelegate> delegate;

- (void)downloadJSONAboutStickers;

@end
